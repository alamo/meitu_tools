#!/bin/bash


# $1 is kernel path
function changeSrcToObj()
{

    echo "cp out/target/product/meitu6797_6c_m/obj/KERNEL_OBJ/${1} kernel-3.18/${1} -af"
    cp out/target/product/meitu6797_6c_m/obj/KERNEL_OBJ/${1}/* kernel-3.18/${1} -af
    
    cd kernel-3.18/${1}
    ##rm file
    DELETE_FILE=`find  -name "*.order" -o -name "*.c" -o -name "*.h" -o -name "built-in.o*"`    
    echo "deleteFile ${DELETE_FILE}"
    for file in ${DELETE_FILE}
    do 
        rm $file
    done

    ##rename file
    RENAME_FILE=`find  -name "*.o"`
    echo ${RENAME_FILE}
    for file in ${RENAME_FILE}
    do
        echo "mv ${file} ${file}_shipped"
        mv ${file} ${file}_shipped
    done
    
    cd ${ROOT_PATH}
}

####################################################################
ROOT_PATH=$(pwd)

##tar shipped file ##
if [ "$1" == "tar" ]; then
    if [ "x$2" == "x" ];then 
        echo "File name error"
        exit
    fi
    git status -s | grep ?? | awk '{print $2}' | xargs tar -jcvf ../${2}.tar.bz2 
    exit
fi


OBJ_FILE_PATH_LIST=(
    "vendor/meitu/MEIOS_Bsp/fingerprints/" 
    "kernel-3.18/drivers/meitu/input/fpc_tee/"
    "vendor/mediatek/proprietary/trustzone/microtrust/source/platform/mt6797/"    
)


for i in ${OBJ_FILE_PATH_LIST[*]}
do 
    changeSrcToObj ${i}
done 
