#!/usr/bin/python
#coding=utf-8

#################################################################################################
#                                
#                                MarrySysMain v0.3
#    v0.1:
#    说明：遍历kernel_log_ 文件,搜索到的第一个androidtime作为基准android time, 根据此android time 修改
#          kernel time
#    使用方式： 拷贝到mobilelog对应的文件夹下双击运行
#
#    v0.2 
#    1. 修改一些解码的问题
#    2. 修改算法，提高时间的准确性
#
#    v0.3
#    1. 修正ms错误的一个问题
#    2. 增加-merge参数,可以合并多个log文件
#
#################################################################################################

import os
import sys
import getpass
import string
import codecs
import datetime
import time

#Global Parameter
LOG_TIME_LIST = {}

def __get_time_from_log(filename, tag):
    global LOG_TIME_LIST
    f = codecs.open(filename, "r", "utf-8",  'replace')   

    file = f.readlines()
    errline = 0
    for line in file:
        #get android system time
        time_start = 0
        if time_start != -1:                        
            time_str     = line[time_start:time_start+14]    
            #time format 09-08 17:20:22.232829       
            #2016-09-08 17:21:29.482063
            try:                                
                #android_time = datetime.datetime.strptime(time_str, "%m-%d %H:%M:%S")
                android_time = datetime.datetime.strptime(time_str, "%m-%d %H:%M:%S") + datetime.timedelta(seconds=float("0." + line[time_start+15:21])) 
                #print android_time                
                line = tag + " " + line
                if not LOG_TIME_LIST.has_key(android_time):
                    LOG_TIME_LIST[android_time] = line
                else:    
                    LOG_TIME_LIST[android_time] += line
            except ValueError:
                errline+=1                                       
    f.close
    print("finish get time from" + filename)        
    #print LOG_TIME_LIST

def get_time_from_log(search_path, filename_keyword, tag):
    sortpath =  os.listdir(search_path)   
    sortpath.sort()
    for file in sortpath:
        if file.find(filename_keyword) != -1:
            __get_time_from_log(os.path.join(search_path, file), tag)

            
def dict_to_file(dict, filename):    
    wr_file_name = os.path.join(filename)
    wr_file = codecs.open(wr_file_name, 'a+', "utf-8",   'replace')

    for i in dict:    
        wr_file.write(i[1])
        #print i[1]
        
    wr_file.close           
            
#Function Def
def MarrySysMain(path):
    global LOG_TIME_LIST
    get_time_from_log(path, "main_log", "main")
    get_time_from_log(path, "sys_log", "sys ")    
    if len(LOG_TIME_LIST) == 0:
        print "Could Found Time in all logs" 
        exit()
    #sort list by kernel time    
    LOG_TIME_LIST = sorted(LOG_TIME_LIST.items(), key=lambda d:d[0], reverse=False)
    dict_to_file(LOG_TIME_LIST, "MarrySysMain.txt")
    

#### main ####
SEARCH_PATH = "./"
if len(sys.argv) > 1:
    SEARCH_PATH = sys.argv[1]  
    
MarrySysMain(SEARCH_PATH)    
    