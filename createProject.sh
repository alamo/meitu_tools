#!/bin/bash 

ROOT_PATH=$(pwd)

BASE_PROJECT="meitu6797_6c_m"
NEW_PROJECT="meitu6797_maya_6c_m"
COMPANY="meitu"



echo "PRELOADER CLONE"
cd ${ROOT_PATH}
cd vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/
cp -a  ${BASE_PROJECT}   ${NEW_PROJECT}
mv  ${NEW_PROJECT}/${BASE_PROJECT}.mk  ${NEW_PROJECT}/${NEW_PROJECT}.mk
sed -i   s/${BASE_PROJECT}/${NEW_PROJECT}/g  ${NEW_PROJECT}/${NEW_PROJECT}.mk



echo "LK CLONE"
cd ${ROOT_PATH} 
cd vendor/mediatek/proprietary/bootable/bootloader/lk/
cp  project/${BASE_PROJECT}.mk   project/${NEW_PROJECT}.mk
cp -r  target/${BASE_PROJECT}   target/${NEW_PROJECT}
sed -i   s/${BASE_PROJECT}/${NEW_PROJECT}/g    project/${NEW_PROJECT}.mk


echo "KERNEL CLONE"
cd ${ROOT_PATH} 
cd kernel-3.18/
cp  -r  drivers/misc/mediatek/mach/mt6797/${BASE_PROJECT}   drivers/misc/mediatek/mach/mt6797/${NEW_PROJECT}   # mt6755以及下面的arm64需要根据您的平台对应修改
cp  arch/arm64/configs/${BASE_PROJECT}_defconfig   arch/arm64/configs/${NEW_PROJECT}_defconfig
cp  arch/arm64/configs/${BASE_PROJECT}_debug_defconfig   arch/arm64/configs/${NEW_PROJECT}_debug_defconfig  
sed  -i  s/${BASE_PROJECT}/${NEW_PROJECT}/g  arch/arm64/configs/${NEW_PROJECT}_defconfig
sed  -i  s/${BASE_PROJECT}/${NEW_PROJECT}/g  arch/arm64/configs/${NEW_PROJECT}_debug_defconfig
cp  arch/arm64/boot/dts/${BASE_PROJECT}.dts   arch/arm64/boot/dts/${NEW_PROJECT}.dts

 
 
#clone android
echo "ANDROID CLONE"
cd ${ROOT_PATH}
cp  -af  device/${COMPANY}/${BASE_PROJECT}   device/${COMPANY}/${NEW_PROJECT}
mv  device/${COMPANY}/${NEW_PROJECT}/full_${BASE_PROJECT}.mk  device/${COMPANY}/${NEW_PROJECT}/full_${NEW_PROJECT}.mk
cp  -af  vendor/mediatek/proprietary/custom/${BASE_PROJECT}  vendor/mediatek/proprietary/custom/${NEW_PROJECT}
cp  vendor/mediatek/proprietary/trustzone/custom/build/project/${BASE_PROJECT}.mk vendor/mediatek/proprietary/trustzone/custom/build/project/${NEW_PROJECT}.mk
cp  -af vendor/${COMPANY}/libs/${BASE_PROJECT} vendor/${COMPANY}/libs/${NEW_PROJECT}
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g device/${COMPANY}/${NEW_PROJECT}/AndroidProducts.mk
sed -i  s/${BASE_PROJECT}/${NEW_PROJECT}/g  device/${COMPANY}/${NEW_PROJECT}/BoardConfig.mk
sed -i  s/${BASE_PROJECT}/${NEW_PROJECT}/g  device/${COMPANY}/${NEW_PROJECT}/device.mk
sed -i  s/${BASE_PROJECT}/${NEW_PROJECT}/g  device/${COMPANY}/${NEW_PROJECT}/full_${NEW_PROJECT}.mk
sed -i  s/${BASE_PROJECT}/${NEW_PROJECT}/g  device/${COMPANY}/${NEW_PROJECT}/vendorsetup.sh
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g  vendor/mediatek/proprietary/custom/${NEW_PROJECT}/security/efuse/input.xml
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g vendor/mediatek/proprietary/custom/${NEW_PROJECT}/Android.mk


#dws sync
echo "DWS CLONE"
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g  vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${NEW_PROJECT}/dct/dct/codegen.dws
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g  vendor/mediatek/proprietary/bootable/bootloader/lk/target/${NEW_PROJECT}/dct/dct/codegen.dws
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g kernel-3.18/drivers/misc/mediatek/mach/mt6797/${NEW_PROJECT}/dct/dct/codegen.dws 
sed -i s/${BASE_PROJECT}/${NEW_PROJECT}/g  vendor/mediatek/proprietary/custom/${NEW_PROJECT}/kernel/dct/dct/codegen.dws

#tiny System
cp vendor/mediatek/proprietary/tinysys/freertos/source/project/CM4_A/mt6797/${BASE_PROJECT} vendor/mediatek/proprietary/tinysys/freertos/source/project/CM4_A/mt6797/${NEW_PROJECT} -af
