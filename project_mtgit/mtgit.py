#!/usr/bin/python
#coding=utf-8

import commands
import os
import sys
import datetime
import pickle
import shutil
import string
import getopt

## const variable
MODIFY_STR="modified:"
NEWFILE_START_STR="Untracked files:"
DELETE_STR="deleted:"
CFG_FILE=".commit_cfg"            #兼容mtsvn的config文件,保持同一个编译脚本可以适应两套
## dirs
CURRENT_WORKING_PATH=os.getcwd()
BASE_PATH="MTK BASECODE PATH"
DRV_PATH="DRV ONLY SVN PATH"
ROM_PATH="MEITU ROM SVN PATH"
PROJECT_TYPE_STR="PROJECT TYPE (drv/rom)"
LIST_DIRS=[ BASE_PATH, DRV_PATH, ROM_PATH, PROJECT_TYPE_STR ]
CFG_DIRS={}

def show_str(str):
        print "\033[1;31;40m" + str  + "\033[0m"
        
def show_err(str):
        print "\033[1;31;40m" + str  + "\033[0m"


## do init things
def env_init():
        dirs={}
        if os.path.exists(CFG_FILE) == False:
                show_str("env is not set! Pls set it first")
                for i in LIST_DIRS:
                        content = raw_input("Please input " + i + " :")
                        if (content != "" and os.path.exists(content)) or (content == "null"):
                                dirs[i] = content                        
                        elif i == PROJECT_TYPE_STR:        
                                dirs[i] = content
                        else:
                                show_str("error! invaild value")
                                return -1
                                
                ##write config file
                cfgfile = open(CFG_FILE, "wb")
                pickle.dump(dirs, cfgfile)
                cfgfile.close()        
        else:
                ##load config file
                cfgfile = open(CFG_FILE, "rb")
                dirs = pickle.load(cfgfile)
                cfgfile.close()
                
        return dirs

class CmdError(ValueError):
    pass
        
def run_command(cmd, showLog=True):
    if showLog:
        show_str(cmd)
    (status, output)=commands.getstatusoutput(cmd)
    if (status != 0):        
        raise CmdError("do " + cmd + " error! " + output)
    if showLog:    
        show_str(output + "\n\r")
    return status, output
    
def run_command_list(cmd_list):
    for cmd in cmd_list:        
        run_command(cmd)

def check_new_file(svn_repository_path, commit_file_dirt):
    copy_file_list=[]
    for key in commit_file_dirt.keys():
        #在SVN中不存在并且在BASE CODE中存在的文件被认为是基础文件
        svn_repository_file = os.path.join(svn_repository_path, key)
        base_repository_file = os.path.join(CFG_DIRS[BASE_PATH], key) 
        if os.path.exists(base_repository_file) and not os.path.exists(svn_repository_file):
            copy_file_list.append(key)
            #拷贝原始文件到目标路径
            if not os.path.exists(os.path.dirname(svn_repository_file)):
                os.makedirs(os.path.dirname(svn_repository_file))
            shutil.copyfile(base_repository_file, svn_repository_file)

    if len(copy_file_list) != 0:                   
        show_str("Need Copy new file!!!")
        print copy_file_list
        return (len(copy_file_list), copy_file_list)
        
    return (len(copy_file_list), copy_file_list)

def do_svn_update(svn_repository_path):
    try:
        origin_path = os.getcwd()
        os.chdir(svn_repository_path)
        run_command("svn update")    
    except CmdError as e:
        print("CmdError: ", e)
    finally:
        os.chdir(origin_path)      
    
def do_svn_add(svn_repository_path, file_list):
    try:
        origin_path = os.getcwd()
        os.chdir(svn_repository_path)
        if len(file_list) <= 0:
            return    
        cmd = "svn add --force"
        for file in file_list:
            cmd += (" " + file)      
        run_command(cmd)            
    except CmdError as e:
        print("CmdError: ", e)
    finally:
        os.chdir(origin_path)    
    
def do_svn_commit(svn_repository_path, file_list):
    try:
        origin_path = os.getcwd()
        os.chdir(svn_repository_path)
        cmd = "svn add"
        for file in file_list:
            cmd += (" " + file)
        run_command_list(("pwd", cmd, "svn commit -m \"[NEW] 提交原始文件\""))        
    except CmdError as e:
        print("CmdError: ", e)
    finally:
        os.chdir(origin_path)
    
def do_commit():
    (current_branch, parent_branch, parent_repository) = do_check_branch()
    if len(sys.argv) > 2 and sys.argv[2] == "continue":
        pass    # mtgit commit continue 跳过update的流程直接进行提交
    else:
        do_update()        
    try:
        run_command("git branch current_work_branch_commit")
        (status, output) = run_command("git diff --raw current_work_branch_commit " + parent_branch + " | awk '{ print $5\" \"$6 }'")
        sp_output = output.splitlines()
        commit_file_dirt={}
        for line in sp_output:
            line = line.split()            
            if line[1] != ".topdeps" and line[1] != ".topmsg":
                commit_file_dirt[line[1]] = line[0]            
        print commit_file_dirt        
        
        if len(commit_file_dirt) <= 0:
            show_err("没有文件需要提交")
            exit()
        
        #升级对应的SVN仓库
        do_svn_update(parent_repository)
        #Todo 检查当前分支的svn版本号与服务器最新的版本号是否一致
        
        #检测并根据需要拷贝原始文件并自动提交
        (file_list_len, file_list) = check_new_file(parent_repository, commit_file_dirt)
        if(file_list_len != 0):
            do_svn_commit(parent_repository, file_list)
            
        raw_input("请检查原始文件是否已经自动拷贝,一共有"+str(file_list_len)+"个原始文件")    
            
        #拷贝修改的文件到svn,并自动执行svn add新增的文件,并提示用户提交/自动提交
        for key in commit_file_dirt.keys():
            svn_repository_file = os.path.join(parent_repository, key)
            if not os.path.exists(os.path.dirname(svn_repository_file)):
                os.makedirs(os.path.dirname(svn_repository_file))
            #print "copy " + key + " to " + svn_repository_file
            #run_command("cp " + key + " " + svn_repository_file)
            shutil.copyfile(key, svn_repository_file)
        do_svn_add(parent_repository, commit_file_dirt.keys())        
    except CmdError as e:
        print("CmdError: ", e)
    finally:
        (st, branchs)=run_command("git branch", False)
        if "current_work_branch_commit" in branchs:
            run_command("git branch -D current_work_branch_commit", False)

        
def do_diff():
    pass

def do_check_branch():
    #查找本分支的原始分支
    (st, current_branch)=run_command("git symbolic-ref --short -q HEAD")
    if os.path.exists(".topdeps"):  #利用tg上的分支依赖      
        (st, parent_branch)=run_command("cat .topdeps")
        if ("svnMeios" in parent_branch):            
            parent_repository=CFG_DIRS[MEIOS_PATH]
        else:                        
            parent_repository = CFG_DIRS[DRV_PATH]            
    else: 
        if ("svnMeios" in current_branch):
            parent_branch="svnMeios"
            parent_repository=CFG_DIRS[MEIOS_PATH]
        else:
            #parent_branch = "svnDriverOnly"
            parent_branch = "svnDriverOn_V2_64_PRE_P76"
            parent_repository = CFG_DIRS[DRV_PATH]
    #检查当前是否在工作分支
    if current_branch == parent_branch:
        show_err("You are not in a work branch, should not commit!!")
        exit()        
    return (current_branch, parent_branch, parent_repository)
    
def do_update():
    (current_branch, parent_branch, parent_repository) = do_check_branch()    
    try:
        #Todo 检查本地是否有冲突未合并
    
        if len(sys.argv) > 2 and sys.argv[2] != "-f":
            (st, current_branch)=run_command("git status -s")
            if len(current_branch) != 0:
                show_err("Found File Not Commit to local git, Please commit it first!!")
                exit()
        
        #检查当前的修改是否有删除base包文件,有的话提示错误并中止提交流程
        run_command("git branch current_work_branch_commit")
        (status, output) = run_command("git diff --raw current_work_branch_commit " + parent_branch + " | awk '{ print $5\" \"$6 }'")        
        sp_output = output.splitlines()
        delete_base_flag = False
        for line in sp_output:
            line = line.split()            
            if line[0] == "D" and os.path.exists(os.path.join(CFG_DIRS[BASE_PATH], line[1])):
                delete_base_flag = True
                show_err("Error!! " + line[1] + " is exist in base code, But You delete it!!!")   
        run_command("git branch -D current_work_branch_commit")        
        if delete_base_flag:
            exit()
        
        #更新原始分支       
        run_command_list((
            "git checkout " + parent_branch,
            "git pull",
            "git checkout " + current_branch,            
        ))
        #合并原始分支到当前分支
        try:
            run_command("git merge --no-ff " + parent_branch)    
        except CmdError as e:
            if "冲突" in e or "conflict" in e:
                show_err("合并冲突，请先处理合并冲突后运行 mtgit commit continue 命令")
                exit()
    except CmdError as e:
        print("CmdError: ", e)
    finally:
        (st, branchs)=run_command("git branch", False)
        if "current_work_branch_commit" in branchs:
            run_command("git branch -D current_work_branch_commit", False)    
    
def do_set_info():
#LIST_DIRS=[ BASE_PATH, DRV_PATH, ROM_PATH, PROJECT_TYPE_STR ]
#-base BASE_PATH -drv DRV_PATH -rom ROM_PATH -type drv/rom
        setdirs = {}
        opts, args = getopt.getopt(sys.argv[2:], "b:d:r:t:", ["base=", "drv=", "rom=", "type="]) 
        print opts
        print args
        for a,o in opts: 
                print "a = " + a
                if a in ('-b', '--base'): 
                        if (o != "" and os.path.exists(o)) or (o == "null"):
                                base_path = o
                                setdirs[BASE_PATH] = o
                        else:
                                print "SET PATH [" + a + "] not exist"
                                return
                elif a in ('-d', '--drv'):
                        if (o != "" and os.path.exists(o)) or (o == "null"):        
                                drv_path = o 
                                setdirs[DRV_PATH] = o
                        else:
                                print "SET PATH " + a + " ERROR"
                                return
                elif a in ('-r', '--rom'): 
                        if (o != "" and os.path.exists(o)) or (o == "null"):                
                                rom_path = o 
                                setdirs[ROM_PATH] = o
                        else:
                                print "SET PATH " + a + " ERROR"
                                return
                elif a in ('-t', '--type'):
                        type = o
                        setdirs[PROJECT_TYPE_STR] = o
        
        if len(setdirs) == 0:
                print "Set info Error "
                return 
                
        for i in LIST_DIRS:
                print setdirs[i]
                if setdirs[i] == "":
                        return
        
        print setdirs
        
        ##write config file
        cfgfile = open(CFG_FILE, "wb")
        pickle.dump(setdirs, cfgfile)
        cfgfile.close()        
        print "Set info Success!"
    
def do_info():
    pass
    
def do_help():
    print ACTIONS.keys()

ACTIONS = {  
"commit":do_commit,        
"diff":do_diff,        
"update":do_update,
"set_info":do_set_info,
"info":do_info,
"help":do_help
}  

if __name__ == "__main__":
    if len(sys.argv) > 1: 
            if (sys.argv[1] != "set_info" and sys.argv[1] != "genpatch"):
                CFG_DIRS = env_init()
                if CFG_DIRS == -1:
                        exit()
            ACTIONS.get(sys.argv[1])()
            exit()

    do_help()     
