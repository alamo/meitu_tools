#!/bin/bash

declare -A REPOSITORY_PATH_TABLE 

#set -x

##数组参数为需要拷贝/链接到的父目录,
##数组值第一个参数为代码仓库路径，可以是svn/git，脚本自动判断.
###第二个参数为复制方式, COVER或者空代表复制，LINK为创建链接,MOVE同COVER，但是在原位置加上指向当前位置的链接
REPOSITORY_PATH_TABLE=( \
    [.\/]="svn://192.168.40.122/meitu/branches/MT6799/ALPS-MP-N0.MP8-OF.P53.PRE.A_K99V1_64_LWCTG_V_P_INHOUSE"\
)
SVN_PATH="ALPS-MP-N0.MP8-OF.P53.PRE.A_K99V1_64_LWCTG_V_P_INHOUSE"


#REPOSITORY_PATH_TABLE=( \
#    [.\/]="svn://192.168.40.122/meitu/branches/MT6799/ALPS-MP-N0.MP8-OF.P76.PRE.A_K99V2_64_LWCTG_V_P_INHOUSE"\
#)
#SVN_PATH="ALPS-MP-N0.MP8-OF.P76.PRE.A_K99V2_64_LWCTG_V_P_INHOUSE"
#myarray=(${myarray[*] test)
###################################################
PENDING_SVN_UPDATE_LIST=()
ROOT_LOCALPATH=$(pwd)/


#### 函数列表 ####
function get_svn_version_list()
{
    svn log ${REPOSITORY_PATH_TABLE[.\/]} -q --incremental | grep "r* |" | awk '{ print $1 }' | tr -d r
}

function get_current_svn_version()
{
    cd ${ROOT_LOCALPATH}/alps
    #if [ -d .svn/ ]; then 
    #    svn log -q -l 1 --incremental | grep "r* |" | awk '{ print $1 }' | tr -d r
    #else 
        for version_str in `git log --pretty=format:"%s" | grep -E "r* \||INIT||Merge||[CHG]"  | awk '{ print $1 }' | tr -d 'r'`
        do
            if [[ $version_str =~ "INIT" ]]; then 
                echo 0    
            elif [[ $version_str =~ "Mege" ]];then                
                continue
            elif [[ $version_str =~ "[CHG]" ]];then                
                continue
            else     
                echo $version_str
                break
            fi
        done
    #fi        
    cd ${ROOT_LOCALPATH}
}

function get_svn_tag_by_version()
{
    svn log ${REPOSITORY_PATH_TABLE[.\/]} -r $1 --incremental | grep -v "^-------------"
}

#### main ####
SVN_LIST=`get_svn_version_list`
CURRENT_SVN_VER=`get_current_svn_version`
echo "CURRENT_SVN_VER = $CURRENT_SVN_VER"

#获取需要更新的版本号列表
if [ "x$CURRENT_SVN_VER" == "x" ];then
     echo "Get CURRENT_SVN_VER=${CURRENT_SVN_VER} Error,exit"
     exit
fi

for ver in $SVN_LIST
do
    if [ $CURRENT_SVN_VER -eq $ver ]; then
        #echo "$CURRENT_SVN_VER 等于 $ver"
        break
    elif [ $CURRENT_SVN_VER -gt $ver ]; then
        #echo "$CURRENT_SVN_VER 大于 $ver"
        break
    else     
        PENDING_SVN_UPDATE_LIST=(${ver} ${PENDING_SVN_UPDATE_LIST[*]})
        #echo "$CURRENT_SVN_VER 小于 $ver"        
    fi
done

echo "当前版本差异${#PENDING_SVN_UPDATE_LIST[*]}个 ${PENDING_SVN_UPDATE_LIST[*]}"
num=${#PENDING_SVN_UPDATE_LIST[*]}
typeset -i num
if [ 0 -gt $num ];then
    exit
fi
#exit

for ver in ${PENDING_SVN_UPDATE_LIST[*]}
do
    echo "开始更新到版本${ver}"
    #更新svn库到对应版本
    cd ${ROOT_LOCALPATH}/${SVN_PATH}
    svn update -r ${ver}

    #拷贝svn库到git
    cd ${ROOT_LOCALPATH}/
    
    #Todo 根据SVN当前更新的文件列表来拷贝文件,而不是全部拷贝
    echo "开始拷贝文件"
    cp ${SVN_PATH}/* alps -af
    
    #提交git
    cd ${ROOT_LOCALPATH}/alps
    #if [ $ver == "42966" ];then        
    #    echo "版本42966，执行删除原始文件操作"
    #    rm -rf external/libnfc-nci
    #    rm -rf frameworks/base/core/java/android/nfc
    #    rm -rf packages/apps/Nfc        
    #fi
    echo "准备提交"    
    GIT_COMMIT_LOG=`get_svn_tag_by_version ${ver}`
    git add -A
    git commit -am "${GIT_COMMIT_LOG}"
    #echo "git commit -am \"${GIT_COMMIT_LOG}\""
    
    #Todo 检查git中本次提交的文件列表和svn当前版本修改的文件列表是否一致    
done 

echo "本地更新完毕,同步到gitlab代码库"
cd ${ROOT_LOCALPATH}/alps
git push --all

