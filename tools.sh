#!/bin/bash  

#adb cmd
function alog()
{
    if [ "x$1" == "x" ]; then
        adb logcat | tee ~/meitu/log/android_log_`date +%Y%m%d_%H%M_%S`
    else
        adb logcat | tee ~/meitu/log/android_log_`date +%Y%m%d_%H%M_%S` | grep --color -E "$1"
    fi    
}

function klog()
{
    TIME_STR=`date +%Y%m%d_%H%M_%S`
    #adb reboot
    adb wait-for-device
    adb root
    adb wait-for-device

    if [ "x$1" == "x" ]; then
        while true
        do
            adb shell cat /proc/kmsg | tee -a ~/meitu/log/kernel_log_${TIME_STR}
        done
    else
        while true
        do
            adb shell cat /proc/kmsg | tee -a ~/meitu/log/kernel_log_${TIME_STR} | grep --color -E "$1"
        done
    fi  
}

#microtrust cmd
function remount_device()
{
    adb root
    adb wait-for-device
    adb remount
}

function push_ifaa()
{
    remount_device

    if [ "x$1" != "x" ];then
        TA_NAME=$1
    else
        TA_NAME=fp_server
    fi

    adb push ${TA_NAME}    /system/thh/alipayapp
    adb push ${TA_NAME}    /data/thh/tee_01/tee

    adb shell sync
}

function push_soter()
{
    remount_device

    if [ "x$1" != "x" ];then
        SOTER_NAME=$1
    else
        SOTER_NAME=soter.raw
    fi

    adb push ${SOTER_NAME}    /system/thh/soter.raw
    adb push ${SOTER_NAME}   /data/thh/system/soter.raw

    adb shell sync
}

function push_ta()
{
    remount_device

    if [ "x$1" != "x" ];then
        TA_NAME=$1
    else
        TA_NAME=fp_server
    fi

    adb push ${TA_NAME}    /system/thh/fp_server
    adb push ${TA_NAME}    /data/thh/tee_05/tee

    adb shell sync
}

function push_uTagent()
{
    if [ "x$1" != "x" ];then
        TA_NAME=$1
    else
        TA_NAME=fp_server
    fi

    adb push ${TA_NAME}    /system/thh/uTAgent
    adb push ${TA_NAME}    /data/thh/tee_00/tee

    adb shell sync
}

function aw9136_autocali()
{
    adb root
    adb wait-for-device 

    echo "=========origin value========="
    adb shell cat /sys/bus/i2c/devices/0-002c/getreg
    adb shell cat /sys/bus/i2c/devices/0-002c/rawdata

    S2_ORIGIN_VALUE=`adb shell cat /sys/bus/i2c/devices/0-002c/getreg | awk '{ print $91 }'`
    S2_ORIGIN_VALUE=`echo ${S2_ORIGIN_VALUE:2:2}  | tr -d '\r' | tr -d '\n'`

    for i in 0 1 2 3 4 5 6 7 8 9 A B C D E F
    do 	
        #echo "0x14 0x${S2_ORIGIN_VALUE}1$i"
        adb shell "echo \"0x14 0x${S2_ORIGIN_VALUE}1$i\" > /sys/bus/i2c/devices/0-002c/getreg"
        adb shell cat /sys/bus/i2c/devices/0-002c/rawdata
    done
}

function systrace()
{
    cd  /home/zhangbin/Android/Sdk/platform-tools/systrace
    python systrace.py --time=2 -o ~/meitu/trace_`date +%Y%m%d_%H%M_%S`.html sched $1
}


function dumpfunc()
{   
    IMAGE_FILE=vmlinux
    OBJDUMP_CMD=aarch64-linux-android-objdump
    while getopts "icf:" arg #选项后面的冒号表示该选项需要参数
    do
        case $arg in
            i)
                IMAGE_FILE=$OPTARG
            ;; 
            
            f)
                FUNCTION_NAME=$OPTARG
            ;;
            
            c)
                OBJDUMP_CMD=$OPTARG
            ;;            
            ?) #当有不认识的选项的时候arg为?
              echo "unkonw argument"
              exit 
            ;;
        esac
    done    
    
    if [ "x$FUNCTION_NAME" == "x" ]; then
        echo "Please input function name as -ffunction_name"
        exit
    fi
    
    START_ADDRESS=`${OBJDUMP_CMD} -x ${IMAGE_FILE} | grep "$FUNCTION_NAME" | awk '{ print $1 }'`
    FUNC_LEN=`${OBJDUMP_CMD} -x ${IMAGE_FILE} | grep $FUNCTION_NAME | awk '{ print $5 }'`    
    END_ADDRESS=`printf "0x%x\n" $((16#${START_ADDRESS}+16#${FUNC_LEN}))`
    
    ${OBJDUMP_CMD} -d ${IMAGE_FILE} --start-address=0x${START_ADDRESS} --stop-address=${END_ADDRESS}
}

