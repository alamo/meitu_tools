#!/bin/bash



function led_enable()
{
    adb shell "echo 10 > /sys/class/leds/button-backlight0/brightness"
    adb shell "echo 10 > /sys/class/leds/button-backlight1/brightness"
    adb shell "echo 10 > /sys/class/leds/button-backlight/brightness"
    adb shell "echo 10 > /sys/class/leds/breath-led/brightness"
}

function led_disable()
{
    adb shell "echo 0 > /sys/class/leds/button-backlight0/brightness"
    adb shell "echo 0 > /sys/class/leds/button-backlight1/brightness"
    adb shell "echo 0 > /sys/class/leds/button-backlight/brightness"
    adb shell "echo 0 > /sys/class/leds/breath-led/brightness"
}

function led_reset()
{
    led_disable
    led_enable
}


################# main #################
LED_BRIGHTNESS_SET="5 10 5 14 5 10"

if [ "x$1" != "x" ]; then
    if [ "$1" == "loop" ]; then
        echo "loop"
    elif [ "$1" == "show" ]; then
        adb shell "cat /sys/mtinfo/led_level"
    else
        LED_BRIGHTNESS_SET=$1
        adb shell "echo ${LED_BRIGHTNESS_SET}  >/sys/mtinfo/led_level"
        led_reset
    fi
fi

