#!/bin/bash

function wait_device_connected()
{
    while true
    do
	adb wait-for-devices
        if [ $? == 0 ];then
	    break
	fi
    done
}


DATE_STR=`date +%Y%m%d_%H%M`
SYSTRACE_TARGET_PATH=~/meitu/log/systrace/${DATE_STR}
mkdir -p ${SYSTRACE_TARGET_PATH}


#wait_device_connected

SYSTRACE_CMD="systrace.py -t 5 -b 20480 -o ${SYSTRACE_TARGET_PATH}/trace.html gfx input view sched freq idle"
echo "${SYSTRACE_CMD}"
eval ${SYSTRACE_CMD}


adb shell dumpsys meminfo  >${SYSTRACE_TARGET_PATH}/dumpsys_meminfo.txt
adb shell cat proc/meminfo >${SYSTRACE_TARGET_PATH}/proc_meminfo.txt
adb shell procrank         >${SYSTRACE_TARGET_PATH}/procrank.txt
adb shell dumpsys gfxinfo  >${SYSTRACE_TARGET_PATH}/gfxinfo.txt

