#!/bin/bash

while true
do
    birghtness_value=`adb shell cat /sys/class/leds/lcd-backlight/brightness`
    value=`adb shell cat /sys/class/leds/lcd-backlight/brightness`
	if [ "x$birghtness_value" == "x" ];then
		continue
	fi
	value=`echo $value | tr -d '\r' | tr -d '\n'`
    radio=`echo "$value*100/255" | bc`
    echo "${value}, ${radio}%"
	sleep 1
done
