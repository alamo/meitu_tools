#!/usr/bin/python
#coding=utf-8

#################################################################################################
#                                
#                                LogTime v0.3
#    v0.1:
#    说明：遍历kernel_log_ 文件,搜索到的第一个androidtime作为基准android time, 根据此android time 修改
#          kernel time
#    使用方式： 拷贝到mobilelog对应的文件夹下双击运行
#
#    v0.2 
#    1. 修改一些解码的问题
#    2. 修改算法，提高时间的准确性
#
#    v0.3
#    1. 修正ms错误的一个问题
#    2. 增加-merge参数,可以合并多个log文件
#
#################################################################################################

import os
import sys
import getpass
import string
import codecs
import datetime
import time

#04-29 21:31:21.194   647   676 I ActivityManager: Android time :[2014-04-29 21:31:21.169] [30314.334]
ANDROID_TIME_STR="android time" 
KERNEL_FILE_NAME="kernel.log"  
ANDROID_TIME_PREFIX="AndroidTime_"
ANDROID_TIME_MERGE_FILE_NAME = ANDROID_TIME_PREFIX + "kernel_log_merge"

PARA_NEED_MERGE = False

KLOG_TIME_LIST = {}      # kernel time & android time list from kernel_log
ALOG_TIME_LIST = {}      # kernel time & android time list from android_log
TAGS_LIST = []

class TIME_PARI:
    def __init__(self):
        self.android_time = 0
        self.kernel_time  = 0
        
        
def get_str_between(source, head, end):
    head_str = source.find(head);
    if head_str == -1:
#        print ("not foune head")
        return -1
                
    st = head_str + len(head)
    end = source[st:-1].find(end)
    if end != -1:        
        return source[st:st+end]
    else:
        return -1
            
            
def get_kernel_time_from_line(line):
    ktim_str = get_str_between(line ,">[", "]")   
    if ktim_str == -1:
        return -1   
    return float(ktim_str)
    


##parse android log file to get android time to kernel time
def __get_androidtime_from_klog(filename):
    f = codecs.open(filename, "r", "utf-8")    
    while 1:        
        line = f.readline()
        if not line:
            break
        #get android system time
        time_start = line.find(ANDROID_TIME_STR) 
        if time_start != -1:            
            time_start  += len(ANDROID_TIME_STR) + 1
            time_str     = line[time_start:time_start+19]        
            android_time = datetime.datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S") + datetime.timedelta(seconds=float("0." + line[time_start+20:-1])) 
            kernel_time  = get_kernel_time_from_line(line)
#            print ("we get android time %s kernel time = %f from %s"  %(android_time, kernel_time, filename))    
            KLOG_TIME_LIST[kernel_time] = android_time    

    f.close

## find android time from klog
def get_androidtime_from_klog(path):
    sortpath =  os.listdir(path)   
    sortpath.sort()
    for file in sortpath:
        if file.find("kernel_log") != -1:
            if not ANDROID_TIME_PREFIX in file:
                print ("check file %s" %(file))
                __get_androidtime_from_klog(os.path.join(path, file))


def ktime_to_android_time(ktime):
    for i in KLOG_TIME_LIST:
        if i[0] > ktime:
            if KLOG_TIME_LIST.index(i) > 0:
                i = KLOG_TIME_LIST[KLOG_TIME_LIST.index(i) - 1]
                break;
        elif i[0] == ktime:
            break;    
    diff = ktime - i[0]
    return i[1] + datetime.timedelta(seconds=diff)

def __set_androidtime_to_klog(filename):
    wr_file_name = ""
    if PARA_NEED_MERGE:
        wr_file_name = os.path.join(ANDROID_TIME_MERGE_FILE_NAME)
        wr_file = codecs.open(wr_file_name, 'a+', "utf-8")
    else:    
        wr_file_name = os.path.join(ANDROID_TIME_PREFIX + os.path.basename(filename))
        wr_file = codecs.open(wr_file_name, 'w+', "utf-8")
        
    f = codecs.open(filename, 'r', "utf-8")
    while 1:
        line = f.readline()
        if not line:
            break;
        ktime = get_kernel_time_from_line(line)
        if ktime != -1:
            and_time = ktime_to_android_time(ktime)
            prefix = "[ Android " + datetime.datetime.strftime(and_time, "%Y-%m-%d %H:%M:%S") + ".%06d"%(and_time.time().microsecond) + " ] "
        else:
            prefix  = ""                
        wr_file.write(prefix + line)    
    f.close
    wr_file.close
    
def set_androidtime_to_klog(path):
    sort_path = []
    ori_path = os.listdir(path)   
    for file in ori_path:
        if not ANDROID_TIME_PREFIX in file:     
            if "kernel_log" in file:
                sort_path.append(file)
     
    sort_path.sort(key=None, reverse=False) 

    if PARA_NEED_MERGE:
        try:
            os.remove(os.path.join(path, ANDROID_TIME_MERGE_FILE_NAME))
        except:
            print ("Not found " + ANDROID_TIME_MERGE_FILE_NAME)

    for file in sort_path:
        __set_androidtime_to_klog(os.path.join(path, file))
    return 
            
def find_in_list(myList,value): 
    try: 
        pos=-1 
        for v in range(0,len(myList)): 
            if value==myList[v]: 
                pos=v 
                break; 
        return pos 
    except: 
        print "find_in_list() Exception!" 
	return -1
		
##parse android log file to get android time to kernel time
def get_all_log_tags(filename):
    f = codecs.open(filename, "r", "utf-8")    
    linenum=0
    while 1:
        line = f.readline()
        if not line:
            break        
        wordlist = line.split(" ")
        if len(wordlist) <= 9:
            continue
        word = wordlist[9]
        if find_in_list(TAGS_LIST, word) == -1: ##not found in tag list
            TAGS_LIST.append(word)
    f.close			
    return TAGS_LIST

def save_all_log_tags(filename, taglist):
    wr_file_name = os.path.join(os.getcwd(), filename)
    wr_file = codecs.open(wr_file_name, 'w+', "utf-8")
        
    for i in taglist:
        wr_file.write(i+"\n")    
    wr_file.close
    
#### main ####

SEARCH_PATH = "./"
if len(sys.argv) > 1:
    SEARCH_PATH = sys.argv[1] 
	
get_all_log_tags(SEARCH_PATH)
#for line in TAGS_LIST:
#    print line
save_all_log_tags(SEARCH_PATH + ".tags", TAGS_LIST)    
exit()
	
#get android time list    
get_androidtime_from_klog(SEARCH_PATH)
if not KLOG_TIME_LIST:
    print ("Could not found \"android time\" in kernel log!!!")
    time.sleep(3)
    exit()

#sort list by kernel time    
KLOG_TIME_LIST = sorted(KLOG_TIME_LIST.items(), key=lambda d:d[0], reverse=False)

#set android time to kernel log
set_androidtime_to_klog(SEARCH_PATH)     




    