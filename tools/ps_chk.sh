#!/bin/bash

adb root

PS_RAW=`adb shell cat /sys/bus/platform/drivers/als_ps/ps`
PS_ANDROID=`adb shell cat /sys/bus/platform/drivers/als_ps/chk_proximity`

while true
do
	PS_RAW=`adb shell cat /sys/bus/platform/drivers/als_ps/ps | tr -d '\r' | tr -d '\n'`
	#value=`echo $value | tr -d '\r' | tr -d '\n'`
	PS_ANDROID=`adb shell cat /sys/bus/platform/drivers/als_ps/chk_proximity | tr -d '\r' | tr -d '\n'`
	echo -e "ANDROID = $PS_ANDROID, RAW = $PS_RAW"
done
