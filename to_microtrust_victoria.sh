#!/bin/bash

####################################################################
ROOT_PATH=$(pwd)

OUT_FILE_PATH_LIST=(
    "vendor/meitu/MEIOS_Bsp/fingerprints/" 
    "kernel-3.18/drivers/meitu/input/fpc_tee/"
    "vendor/mediatek/proprietary/trustzone/microtrust/source/platform/mt6797/"   
    "kernel-3.18/drivers/misc/mediatek/teei/V1.0/"
)


DATE_STR=`date +%Y%m%d_%H%M`
SVN_STR=`svnversion`

#echo ${OUT_FILE_PATH_LIST[*]} 
TAR_FILE_NAME=to_microtrust_fpc_tee_svn${SVN_STR}_${DATE_STR}.tar.bz2
tar -jcvf ~/meitu/Victoria/${TAR_FILE_NAME} ${OUT_FILE_PATH_LIST[*]}
