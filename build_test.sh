#!/bin/bash


BASE_CODE_DIR="alps"


function git_commit()
{
    git init
    git add -A
    git commit -am "init"
}


##工作目录##
rm test_build -rf
mkdir test_build/
cd test_build/
ROOT_WORK_DIR=$(pwd)

##base code##
BASE_FILE_CONTENT="base content"
mkdir alps/base1 -p
mkdir alps/base2/base22/base222 -p
echo ${BASE_FILE_CONTENT} > alps/base1/1.c
echo ${BASE_FILE_CONTENT} > alps/base2/2.c
echo ${BASE_FILE_CONTENT} > alps/base2/base22/22.c
#
tar -jcvf base_code.tar.bz2 alps
mkdir base_code
mv alps base_code



##create project_cfg
MEITU_TOOL_PATH=$(which mtsvn | sed 's/mtsvn/config_file\/.project_cfg_test/g')
cp ${MEITU_TOOL_PATH} .project_cfg


#create svn1
mkdir commit_path_1
cd commit_path_1
SVN_FILE_CONTENT="svn content 1"
mkdir alps/base1 -p
mkdir alps/base2/base22/base222 -p
echo ${BASE_FILE_CONTENT} > alps/base1/1.c
echo ${SVN_FILE_CONTENT} >> alps/base1/1.c
echo ${BASE_FILE_CONTENT} > alps/base2/2.c
echo ${SVN_FILE_CONTENT} >> alps/base2/2.c
echo ${BASE_FILE_CONTENT} > alps/base2/base22/22.c
echo ${SVN_FILE_CONTENT} >> alps/base2/base22/22.c
cd alps
git_commit
cd ${ROOT_WORK_DIR}

#create svn2
mkdir commit_path_2
cd commit_path_2
SVN_FILE_CONTENT2="svn content 2"
mkdir meios/
mkdir meios/meiosbase1 -p
mkdir meios/meiosbase2/meiosbase22/ -p
echo ${SVN_FILE_CONTENT2} > meios/meiosbase1/meios1.c
echo ${SVN_FILE_CONTENT2} > meios/meiosbase2/meios2.c
echo ${SVN_FILE_CONTENT2} > meios/meiosbase2/meiosbase22/meios22.c
cd meios/
git_commit
cd ${ROOT_WORK_DIR}















