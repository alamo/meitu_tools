#!/bin/bash


DEBUG_DIR=debug_`date +%Y%m%d_%H%M`
mkdir ${DEBUG_DIR}
cd ${DEBUG_DIR}

adb shell "ps -t | grep \" D \"" > ps.log
adb shell dumpstate > dumpstate.log
adb shell dumpsys > dumpsys.log