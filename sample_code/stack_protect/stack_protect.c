#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>


#define MB_SIZE_LEN         (1024*1024)
#define ARRAY_SIZE(x)       (sizeof(x)/sizeof((x)[0]))

typedef struct FUNCTION_LIST
{
    char * const name;
    int (*func)(void);
}FUNCTION_LIST_STRU;


static void void_fun_sample(void)
{
    printf("I am void_fun_sample\n");
}

static int int_fun(void)
{
    int ret = 0;

    printf("I am int_fun_sample\n");
    return ret;
}

static int __para_fun(int p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8)
{
    p1 = 1;
    p2 = 2;
    p3 = 3;
    p4 = 4;
    p5 = 5;
    p6 = 6;
    p7 = 7;
    p8 = 8;

    printf("I am para_fun_sample\n");
    return p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8;
}
static int para_fun(void)
{
    __para_fun(7, 6, 5, 4, 3, 2, 1, 0);
    return 0;
}

static void __heap_overflow(char *p_overflow_me)
{
    char temp[] = "123456789";

    int i;

    //char *p_overflow_me = malloc(malloc_len);;

    if(!p_overflow_me)
    {
        printf("malloc memory err\n");
        return;
    }
    printf("set __heap_overflow %s\n", temp);
    //sprintf(overflow_me, "123456789ABCDEFGHIJKLMN");
    for(i = 0; i<4000; i++)
    {
        printf("begin copy i = %d, p_overflow_me = %p\n", i, p_overflow_me);
        memset(p_overflow_me, 0xAA, MB_SIZE_LEN);
        printf("end copy i = %d, p_overflow_me = %p\n", i, p_overflow_me);
        p_overflow_me += MB_SIZE_LEN;
    }
    //sprintf(p_overflow_me, "%s", temp);
    //free(p_overflow_me);
}
static int heap_overflow(void)
{
    int malloc_len = MB_SIZE_LEN;
    char *p_malloc = malloc(malloc_len);

    __heap_overflow(p_malloc);
    printf("heap_overflow finish\n");
    free(p_malloc);
    return 0;
}

static int use_after_free(void)
{
    void *p = malloc(10);
    if(!p)
    {
        printf("malloc memory err\n");
        return -1;
    }

    memset(p, 0xFF, 10);
    printf("we free!\n");
    free(p);

    //
    printf("we use!\n");
    memset(p, 0xA5, 10);

    return 0;
}

static int double_free(void)
{
    void *p = malloc(10);
    if(!p)
    {
        printf("malloc memory err\n");
        return -1;
    }

    memset(p, 0xA5, 10);

    printf("first free!\n");
    free(p);
    //
    printf("second free!\n");
    free(p);

    return 0;
}

static int stack_overflow_pos(void)
{
    char str1[] = "12345678";
    char str2[] = "abcdefgh";
    int pos = 4, i;

    for(i=0; i<1; i++)
    {
        pos = pos + pos;
    }


    memset(str2, 0x30, pos);
    return 0;
}
static int stack_overflow_nav(void)
{
    char str1[] = "12345678";
    char str2[] = "abcdefgh";
    int pos = 4;

    pos = pos * -2;


    memset(&str2[pos], 0x30, 4);
    return 0;
}


static struct FUNCTION_LIST const FUNCTION_LIST_TBL[] =
{
    { "ParaFun",       para_fun           },
    { "IntFun",        int_fun            },
    { "DoubleFree",    double_free        },
    { "HeapOver",      heap_overflow      },
    { "UseAfterFree",  use_after_free     },
    { "StackOverPos",  stack_overflow_pos },
    { "StackOverNav",  stack_overflow_nav },
};


void help(char const * name)
{
    unsigned int i, len;
    char help_str[400];

    len = snprintf(help_str, sizeof(help_str), "%s: [ ", name);

    for(i=0; i< ARRAY_SIZE(FUNCTION_LIST_TBL); i++ )
    {
         len += snprintf(&help_str[len], sizeof(help_str), "%s ", FUNCTION_LIST_TBL[i].name);
    }

    len += snprintf(&help_str[len], sizeof(help_str), "]");
    printf("%s\n", help_str);
}

int main(int argc, char **argv)
{
    unsigned int i;
    int ret;

    if((argc < 2) || (argv[1] == NULL))
    {
        help(argv[0]);
        return 0;
    }

    for(i=0; i< ARRAY_SIZE(FUNCTION_LIST_TBL); i++ )
    {
        if(!strcmp(argv[1], FUNCTION_LIST_TBL[i].name))
        {
            return FUNCTION_LIST_TBL[i].func();
        }
    }

    __asm__("nop");
    __asm__("nop");
    __asm__("nop");

    void_fun_sample();
    return 0;
}

