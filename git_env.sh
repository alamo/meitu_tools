#!/bin/bash

#设置bcompare为代码比较工具
git config --global diff.tool bc3
git config --global difftool.bc3.trustExitCode true 


#设置bcompare为代码合并工具
git config --global merge.tool bc3
git config --global mergetool.bc3.trustExitCode true
