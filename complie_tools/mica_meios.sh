#!/bin/bash
export LANG=en_US.UTF-8

declare -A REPOSITORY_PATH_TABLE REPOSITORY_LINK_CHECKOUT_PATH_TABLE REPOSITORY_COVER_CHECKOUT_PATH_TABLE REPOSITORY_MOVE_CHECKOUT_PATH_TABLE

##数组参数为需要拷贝/链接到的父目录,
##数组值第一个参数为代码仓库路径，可以是svn/git，脚本自动判断.
###第二个参数为复制方式, COVER或者空代表复制，LINK为创建链接,MOVE同COVER，但是在原位置加上指向当前位置的链接
REPOSITORY_PATH_TABLE=( \
    [.\/]="svn://192.168.40.122/meitu/trunk/meios3/mica"\
    [.\/\/]="svn://192.168.40.122/meitu/trunk/meios3/meios MOVE" \
    [.\/\/\/]="svn://192.168.40.122/meitu/trunk/meios3/meios_6.0 MOVE" \
    [.\/\/\/\/]="svn://192.168.40.122/meitu/trunk/meios3/frameworks-meios MOVE" \
)
ORIG_CODE=$(pwd)/mica_m0mp7_v1.5.tar.gz
#ORIG_CODE=$(pwd)/test.tar.bz2

PROJECT_NAME="Mica_rom"

BUILD_TIME="`date +%Y%m%d_%H%M`"
###############################################################

if [ ! -f "$ORIG_CODE" ]; then 

echo "##[error]##############################################"
echo "$ORIG_CODE is miss!"
echo "Pls copy ORIG_CODE from service"
echo "scp meitu@172.16.1.230:/home/mica_m0mp7_v1.5.tar.gz"
echo "password:meitu"
echo "#######################################################"
exit 0
fi 
###############################################################



BUILD_MODE="eng"
BUILD_CMD=./build_eng_all.sh
RELEASE_CMD=./release_eng.sh

ROOT_LOCALPATH=$(pwd)/$PROJECT_NAME"_"$BUILD_TIME


rm -rf $ROOT_LOCALPATH
mkdir $ROOT_LOCALPATH
cd $ROOT_LOCALPATH


REPOSITORY_COVER_PATH=$ROOT_LOCALPATH/repository_cover
REPOSITORY_LINK_PATH=$ROOT_LOCALPATH/repository_link
mkdir -p $REPOSITORY_COVER_PATH $REPOSITORY_LINK_PATH

function get_repository_path()
{
    echo $1 
}
function get_repository_path_property()
{
    if [ "x$2" == "x" ]; then
        echo "COVER"
    else
        echo $2
    fi
    
}
function check_repository_type()
{
     echo "svn"
}

function checkout_repository
{    
    if [ `check_repository_type $1` == "svn" ]; then
        svn checkout $1        
    else
        git clone $1
    fi
}

REPOSITORY_LINK_CHECKOUT_PATH_TABLE=() 
REPOSITORY_COVER_CHECKOUT_PATH_TABLE=()

echo "##[Checkout branch]##############################################"
for i in ${!REPOSITORY_PATH_TABLE[*]}
do
    REPOSITORY_PATH=`get_repository_path ${REPOSITORY_PATH_TABLE[$i]}`
    REPOSITORY_DIR=$(echo ${REPOSITORY_PATH} | awk  'BEGIN { FS="/" } { print $NF }')
    if [ `get_repository_path_property ${REPOSITORY_PATH_TABLE[$i]}` == "COVER" ] ;then
        mkdir -p ${REPOSITORY_COVER_PATH}/$i
        cd ${REPOSITORY_COVER_PATH}/$i
        checkout_repository ${REPOSITORY_PATH}
        REPOSITORY_COVER_CHECKOUT_PATH_TABLE[$i]=${REPOSITORY_COVER_PATH}/$i/${REPOSITORY_DIR}
    elif [ `get_repository_path_property ${REPOSITORY_PATH_TABLE[$i]}` == "LINK" ] ;then
        mkdir -p ${REPOSITORY_LINK_PATH}/$i
        cd ${REPOSITORY_LINK_PATH}/$i
        checkout_repository `get_repository_path ${REPOSITORY_PATH_TABLE[$i]}`
        REPOSITORY_LINK_CHECKOUT_PATH_TABLE[$i]=${REPOSITORY_LINK_PATH}/$i/${REPOSITORY_DIR}
    elif [ `get_repository_path_property ${REPOSITORY_PATH_TABLE[$i]}` == "MOVE" ] ;then
        mkdir -p ${REPOSITORY_LINK_PATH}/$i
        cd ${REPOSITORY_LINK_PATH}/$i
        checkout_repository `get_repository_path ${REPOSITORY_PATH_TABLE[$i]}`
        REPOSITORY_MOVE_CHECKOUT_PATH_TABLE[$i]=${REPOSITORY_LINK_PATH}/$i/${REPOSITORY_DIR}
    fi
done


echo "##[Unzip MTK basecode]#####################################"
cd $ROOT_LOCALPATH
tar -xf $ORIG_CODE

echo "##[Overwrite svn]#####################################"
cd alps

BUILD_PROJECT_ROOT=$(pwd)
for i in ${!REPOSITORY_COVER_CHECKOUT_PATH_TABLE[*]}
do
    echo "$i = ${REPOSITORY_COVER_CHECKOUT_PATH_TABLE[$i]}"
    if [ ! -d $i ]; then
        mkdir -p $i
    fi
    cp -af ${REPOSITORY_COVER_CHECKOUT_PATH_TABLE[$i]}/. $i/
done

for i in ${!REPOSITORY_LINK_CHECKOUT_PATH_TABLE[*]}
do
    echo "$i = ${REPOSITORY_LINK_CHECKOUT_PATH_TABLE[$i]}"
    if [ ! -d $i ]; then
        mkdir -p $i
    fi
    cd $i/
    ln -s ${REPOSITORY_LINK_CHECKOUT_PATH_TABLE[$i]}
    cd ${BUILD_PROJECT_ROOT}
done

for i in ${!REPOSITORY_MOVE_CHECKOUT_PATH_TABLE[*]}
do
    echo "$i = ${REPOSITORY_MOVE_CHECKOUT_PATH_TABLE[$i]}"
    if [ ! -d $i ]; then
        mkdir -p $i
    fi
    cd $i/
    mv ${REPOSITORY_MOVE_CHECKOUT_PATH_TABLE[$i]} .
    _DIRNAME_=$(pwd)/`basename ${REPOSITORY_MOVE_CHECKOUT_PATH_TABLE[$i]}`
    ln -s ${_DIRNAME_}  ${REPOSITORY_MOVE_CHECKOUT_PATH_TABLE[$i]}
    cd ${BUILD_PROJECT_ROOT}
done
#exit

echo "##[Add git]#####################################"
cd ${BUILD_PROJECT_ROOT}
git init
cp ~/meitu/meitu_tools/config_file/.gitignore .
git add -A;git commit -am "[init] init"
git config core.filemode false


cd ${BUILD_PROJECT_ROOT}
chmod 777 *.sh
./build_userdebug_all.sh

echo "--[Done!]-----------------------------------"





pwd
