#/bin/bash

declare -A VERSION_BUILD_CMD_TBL
declare -A VERSION_BUILD_PREBUILD_CMD_TBL
declare -A VERSION_SVN_PATH_TBL


## 参数检测列表 ##
CMD_MUSTCHECK_TBL=( \
    PATH_BASE_CODE \
    TARGET_DIR_PREHEAD \
    BUILDI_DEFAULT_BUILD_CMD \
    PATH_BASE_CODE_DIR_FOR_COMPARE \
 )


#BASE_CODE输出目录的路径,如果有新的路径加到此表中
GUESS_DIR_PATH=( "workspace" "alps" "pre_mt6755" )

## INTERNAL VARS 参数初始化 ##
DOTS_LINE="-----------------------------------------------------------"
IS_DEBUG="false"
FORCE_BUILD="false"
SVN_VERSION=""
TARGET_DIR=""
NEED_BUILD=false
NEED_GIT=false
PROJECT_CONFIG_FILE_NAME=".project_cfg"

function check_valid()
{
    if [ "x$1" == "x" ]; then
        exit
    fi
}

function env_init()
{
    PROJECT_CONFIG_FILE_PATH="$(pwd)/$PROJECT_CONFIG_FILE_NAME"

    ## 判断项目配置文件是否存在
    if [ ! -f ${PROJECT_CONFIG_FILE_PATH} ];then
        shell_err "${PROJECT_CONFIG_FILE_PATH} not exist! Quit!"
        exit
    fi
    
    ## Load PROJECT_CONFIG_FILE_NAME 
    source ${PROJECT_CONFIG_FILE_PATH}    
    
    ##Check Project Parameter Setting!
    for ((i=0; i<${#CMD_MUSTCHECK_TBL[*]}; i++))
        do
            local temp_para=${CMD_MUSTCHECK_TBL[$i]}
            eval para_content=\$${temp_para}
            shell_dbg "check ${temp_para}, echo ${para_content}"
            if [ "x${para_content}" == "x" ]; then
                shell_err "${temp_para} is Not Set!! Quit!!"
                exit
            fi        
        done
        
    MAX_MAKE_NUM=${#VERSION_TBL[*]}*${#BUILD_TYPE_TBL[*]}    
}

function shell_err()
{
    if [ "x$1" != "x" ]; then
        echo -e "\e[1;31m$1\e[0m"
    fi
}

function shell_warn()
{
    if [ "x$1" != "x" ]; then
        echo -e "\e[1;31m$1\e[0m"
    fi
}

function shell_info()
{
    if [ "x$1" != "x" ]; then
        echo -e "\e[1;33m$1\e[0m"
    fi
}

function shell_dbg()
{
    if [ ${IS_DEBUG} == "false" ]; then
        return
    fi

    if [ "x$1" != "x" ]; then
        echo -e "\e[1;31m$1\e[0m"
    fi
}

##  Function Define ##
function get_max_version()
{
    SVN_MAX_VERISON=`svn info ${VERSION_SVN_PATH_TBL[${MAKE_VERSION}]}`
    IS_FOUND_VERSION="False"
    GET_VERSION=""
    
    for line in $SVN_MAX_VERISON
    do    
        if [ $IS_FOUND_VERSION == "true" ]; then
            GET_VERSION=`echo "$line" | tr -d '\r' | tr -d '\n'`
            break
        fi
        if [[ $line == "版本:" || $line == "Revision:" ]] ; then
            IS_FOUND_VERSION="true"
        fi
    done  
    
    echo "${GET_VERSION}"
}

## 检查编译参数 ##
function check_complie_parameter()
{
    while getopts "b:r:d:g:" arg #选项后面的冒号表示该选项需要参数
    do
        case $arg in
             b)
                NEED_BUILD=false
            ;;

             g)
                NEED_GIT=false
            ;;            
            
             v)
                if [ "x$OPTARG" != "x" ] ; then
                MAKE_VERSION=$OPTARG	
                fi
                ;;

             t)
                if [ "x$OPTARG" != "x" ] ; then
                MAKE_BUILD_TYPE=$OPTARG	
                fi
                ;;
                
             r)
                if [ "x$OPTARG" != "x" ] ; then
                SVN_VERSION=$OPTARG	
                fi
            ;;

             d)
                if [ "x$OPTARG" != "x" ] ; then
                TARGET_DIR=$OPTARG	
                fi
                ;;

             ?) #当有不认识的选项的时候arg为?
                shell_err "unkonw argument"
                exit 
                ;;
        esac
    done
}

function setup_basecode()
{
    ## 解压basecode ## 
    shell_info "upackaging basecode......"
    tar -xf ${PATH_BASE_CODE} -C $TARGET_DIR
    check_ok
}

function svn_checkout()
{
        svn checkout -r $SVN_VERSION  ${SVN_PATH} &>${BUILD_LOG}
        check_ok
}

function setup_svn_code()
{
    if [[ ${SVN_VERSION} -eq ${SVN_MAX_VERSION} ]]; then 
        if [ -d ${SVN_DIR} ]; then
            shell_info "svn ${SVN_DIR} exist, update it!"
            cd ${SVN_DIR}
            svn update  &>${BUILD_LOG}
            if [ $? != 0 ] ;then
                shell_err "Update  ${SVN_DIR} Error"
            fi       
            cd ..
        else
            shell_info "svn ${SVN_DIR} not exist, checkout it!"
            svn_checkout
        fi
    else       
        shell_info "Spection SVN Version, checkout it!"
        cd $TARGET_DIR
        svn_checkout
        cd ..
    fi

    ## 创建SVN目录链接
    cd $TARGET_DIR
    ln -s ../${SVN_DIR}
    cd ..    
}

function check_ok()
{
    if [ $? == 0 ]; then
        shell_info "OK!"
    else
        shell_err "ERROR!"
        exit
    fi
}

#function guess_version_type()
#{
#}
function guess_base_code_dir()
{
    for i in ${GUESS_DIR_PATH[*]}
    do
        if [ -d $i ] ;then
            echo "$i"
	    return 
        fi
    done
}

function prepare_code_default()
{
## PARAMETER SET ##
    SVN_DIR=$(echo $SVN_PATH | awk  'BEGIN { FS="/" } { print $NF }')
    mkdir -p $TARGET_DIR

    setup_svn_code
    setup_basecode

    cd $TARGET_DIR    
    BASE_CODE_DIR=$(guess_base_code_dir)
    
    ## 复制版本库到BASE_CODE ## 
    if [[ -d ${SVN_DIR} && -d ${BASE_CODE_DIR} ]] ;then
         shell_info "copy svn_code  to basecode......"
    else
         shell_err "check path error, need ${SVN_DIR} ${BASE_CODE_DIR}, but path now has\n${DOTS_LINE}\n$(ls)\n${DOTS_LINE}\n"
         exit
    fi

    cp ${SVN_DIR}/. ${BASE_CODE_DIR} -af
    check_ok
}

function prepare_code_custom()
{
    chmod 777 ${PREPARE_CODE_SCRIPT}
    eval ${PROJECT_PATH}/${PREPARE_CODE_SCRIPT} $TARGET_DIR ${PROJECT_PATH}/${PATH_BASE_CODE}
    check_ok
}


function prepare_code()
{
    if [ "x${PREPARE_CODE_SCRIPT}" != "x" ]; then
        shell_info "Use ${PREPARE_CODE_SCRIPT} Script  ${PROJECT_PATH}/$TARGET_DIR"
        prepare_code_custom 
        cd ${PROJECT_PATH}/$TARGET_DIR    
        BASE_CODE_DIR=$(guess_base_code_dir)
    else
        shell_info "Use Default Script"
        prepare_code_default
    fi
}


function check_make_input_valid()
{
    if [ "${1}x" == "x" ]; then    
        return 1
    fi   

    for ((i=0,k=1; i<${#VERSION_TBL[*]}; i++))
        do
            for((j=0; j<${#BUILD_TYPE_TBL[*]}; j++,k++))
            do                
                if [[ ""${VERSION_TBL[$i]}-${BUILD_TYPE_TBL[$j]}"" == "${1}" || "${k}" == "${1}" ]]; then
                    MAKE_BUILD_TYPE=${BUILD_TYPE_TBL[$j]}
                    MAKE_VERSION=${VERSION_TBL[$i]}
                    MAKE_NUM="${VERSION_TBL[$i]}-${BUILD_TYPE_TBL[$j]}"
                    return 0
                fi
                #k++
            done
        done
        
    return 1   
}

function show_all_make()
{
    for ((i=0; i<${#VERSION_TBL[*]}; i++))
        do
            for((j=0; j<${#BUILD_TYPE_TBL[*]}; j++))
            do
                shell_info "$[$i*${#BUILD_TYPE_TBL[*]}+$j+1]. ${VERSION_TBL[$i]}-${BUILD_TYPE_TBL[$j]}"
            done
        done
}

################################### Main ###################################

#环境初始化#
env_init

## 检查编译参数 ##
check_complie_parameter

## 确定项目版本 ## 
if [ "${MAKE_VERSION}x" == "x" -o "${MAKE_BUILD_TYPE}x" == "x" ] ;then
    VERSION_STRING="Please input make num"
    shell_info "${VERSION_STRING}"
    show_all_make
    shell_info "Enter your branch:"
    read -a MAKE_NUM  
fi


## 检查BRANCH号是否正确 ## 
check_make_input_valid ${MAKE_NUM}
if [ $? != 0 ]; then
    shell_err "unknow branch, we have  "
    show_all_make
    exit
fi
PROJECT_PATH=$(pwd)
SVN_PATH=${VERSION_SVN_PATH_TBL[${MAKE_VERSION}]}
SVN_MAX_VERSION=$(get_max_version)
if [ "x$SVN_VERSION" == "x" ] ; then
    SVN_VERSION=$SVN_MAX_VERSION
fi

if [ "x$TARGET_DIR" == "x" ] ; then
    DATE_STR=`date +%Y%m%d_%H%M`
    MK_VER=`tr '[a-z]' '[A-Z]' <<<"${MAKE_VERSION}-${MAKE_BUILD_TYPE}"`
    MK_VER=`tr '-' '_' <<<"${MK_VER}"`
    TARGET_DIR="${TARGET_DIR_PREHEAD}_${MK_VER}_SVN${SVN_VERSION}_${DATE_STR}"
fi
TARGET_BASE_PATH=$(pwd)/${TARGET_DIR}/
BUILD_LOG=$(pwd)/${TARGET_DIR}/build.log
## version control check ## 
VERSION_CONTROL=""
if [ ${NEED_GIT} == false ] ;then
    shell_info "Need Version Control By Git:(Y/N)"
    read -a VERSION_CONTROL
    
    if [ ${VERSION_CONTROL} == "y" -o ${VERSION_CONTROL} == "Y" ]; then
        NEED_GIT=true
    fi
fi
shell_info "You Choice List\n${DOTS_LINE}\nBRANCH: ${MAKE_VERSION}-${MAKE_BUILD_TYPE}\nSVN:    $SVN_PATH\nVER:    $SVN_VERSION\nPACH:   $(pwd)/${TARGET_DIR}\nGIT:    ${NEED_GIT}\nBUILD:  ${NEED_BUILD}\n${DOTS_LINE}\n"

#######################################开始编译########################################
prepare_code
cd ${BASE_CODE_DIR}

## 添加版本管理 ## 
if [ ${NEED_GIT} == true ] ;then
    shell_info "Set git......"
    ## 判断并校正GIT CONFIG 路径
    if [ "x${PATH_GIT_CONFIG}" == "x" ] || [ ! -f  ${PATH_GIT_CONFIG} ]  ;then
        shell_info "Use Default .gitignore PATH"
        MKPROJECT_PATH=$(which mkproject)
        PATH_GIT_CONFIG=$(echo "${MKPROJECT_PATH%mkproject}config_file/.gitignore")
    fi    
    
    if [ "x${PATH_GIT_CONFIG}" == "x" ] || [ ! -f  ${PATH_GIT_CONFIG} ]  ;then
        shell_err "PATH_GIT_CONFIG ERROR! <${PATH_GIT_CONFIG}>"
        exit
    fi    
    ## 判断并校正GIT CONFIG 路径
    git init &>${BUILD_LOG}
    cp ${PATH_GIT_CONFIG} .
    ## 忽略文件权限
    git config core.filemode false
    git add -A &>${BUILD_LOG}
    git commit -am "[init] update to svn $SVN_VERSION" &>${BUILD_LOG}
    check_ok
    
    ## 设置提交工具参数 ## 
    shell_info "Set mtsvn......"
    echo "${MAKE_VERSION}" | grep -q "drv"
    if [ $? -eq 0 ]; then
        mtsvn set_info --base ${PATH_BASE_CODE_DIR_FOR_COMPARE} --rom ${TARGET_BASE_PATH}${SVN_DIR}/ --drv null --type=rom &>${BUILD_LOG}
    else
        mtsvn set_info --base ${PATH_BASE_CODE_DIR_FOR_COMPARE} --drv ${TARGET_BASE_PATH}${SVN_DIR}/ --rom null --type=drv &>${BUILD_LOG}
    fi
fi


## 不需要编译，直接退出 ## 
if [ $NEED_BUILD == false ]; then
    shell_info "We dont' need build project, Job Finish!!"
    exit
fi

## 执行编译命令 ## 
if [ "${VERSION_BUILD_CMD_TBL[${MAKE_BUILD_TYPE}]}x" != "x" ] ;then
    shell_info "${VERSION_BUILD_CMD_TBL[${MAKE_BUILD_TYPE}]} "
    eval ${VERSION_BUILD_CMD_TBL[${MAKE_BUILD_TYPE}]} &>${BUILD_LOG}
else
    shell_info "${BUILDI_DEFAULT_BUILD_CMD}"
    eval ${BUILDI_DEFAULT_BUILD_CMD} &>${BUILD_LOG}
fi

if [ $? != 0 ] ;then
    shell_err "do build cmd error"  
else
    shell_info "!!!ok!!!!"        
fi

shell_info "We can check ${BUILD_LOG}\ntail -n 3 ${BUILD_LOG}"
tail -n 3 ${BUILD_LOG}
