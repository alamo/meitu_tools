#!/bin/bash

cd alps

GIT_IGNORE_TEMP_DIR="../gitignore_dir"
rm $GIT_IGNORE_TEMP_DIR -rf
mkdir $GIT_IGNORE_TEMP_DIR

BACKUP_IFS=$IFS

IFS=$(echo -en "\n\b")
for file in `find -name .gitignore`
do
    cp --parents $file $GIT_IGNORE_TEMP_DIR
    #echo "cp --parents $file $GIT_IGNORE_TEMP_DIR"
    rm $file
done    
IFS=${BACKUP_IFS}


git add -A
git commit -am "[ADD] Add Base Code Without all .gitignore"
cp $GIT_IGNORE_TEMP_DIR/. . -af
git add -A
git commit -am "[ADD] Add Base Code"
git tag -a v0.0.1 -m "[INIT] Init MTK BaseCode"
git gc
git push --all 
git push --tags
