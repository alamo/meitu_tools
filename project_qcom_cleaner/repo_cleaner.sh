#!/bin/bash

function init_gitignore()
{
	if [ ! -f .gitignore ];then
		touch .gitignore		
		echo "#COMMON" > .gitignore
		echo "*.pyc" >> .gitignore
		echo "*.o" >> .gitignore
		echo "*.log" >> .gitignore
		echo "*.i" >> .gitignore
		echo "#QCOM" >> .gitignore
	fi
}

function is_common_build_file()
{
	if [[ "x$(echo $1 | grep -E "\.pyc\$|\.o\$|\.log\$|\.i\$")" != "x" ]];then
		echo "true"
	else
		echo "false"
	fi	
}


function add_2_gitignore()
{
	if [ "x$1" == "x" ];then
		return
	fi
	
	init_gitignore
	if [[ $(is_common_build_file $1) == "true" ]];then
		return
	fi
	
	if [[ "x$(cat .gitignore | grep $1)" == "x" ]];then
		echo $1 >> .gitignore
	fi
}
    
function adsp_clean()
{
	git config core.filemode false
	for file in $(git status -s|awk '{print $2}')
	do
		if [ "$file" == ".gitignore" ];then
			continue
		fi	
		rm $file
		git rm $file
		add_2_gitignore $file
	done
	git commit -m "[DEL] 删除无用的编译中间文件"
	git add .gitignore
	git commit -m "[ADD] 更新.gitignore文件"
}

adsp_clean

####main####

#cd AMSS/adsp_proc
#adsp_clean
#cd -