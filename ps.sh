#!/bin/bash 

PROCESS_LIST=`adb shell "ps -t | grep \" D \"" | awk '{print $2}'`

echo $PROCESS_LIST
for i in $PROCESS_LIST
do
    echo "process id is $i"
    adb shell "cat /proc/$i/stack"
    echo "\n"
done
