#!/bin/bash

####################################################################
ROOT_PATH=$(pwd)

OUT_FILE_PATH_LIST=(
    "meios/MEIOS_Bsp/fpc_tee" 
    "vendor/mediatek/proprietary/trustzone"
    "meios/MEIOS_Bsp/device_bsp.mk"
    "kernel-3.18/drivers/misc/mediatek/teei/V2.2.0/"
    "frameworks/base/core/java/android/security/keymaster/KeymasterDefs.java"
    "vendor/mediatek/proprietary/bootable/bootloader/preloader/platform/mt6755/src/security/trustzone/inc/utos_version.h"
    "device/mediatek/common/sepolicy/file_contexts"
    "device/mediatek/common/sepolicy/meitu_keymaster.te"
    "device/mediatek/common/sepolicy/shell.te"
    "device/mediatek/mt6755/init.microtrust.rc"
    "frameworks/base/Android.mk"
    "frameworks/base/core/java/android/app/ActivityThread.java"
    "hardware/libhardware/include/hardware/gatekeeper.h"
    "hardware/libhardware/include/hardware/keymaster_defs.h"
    "system/keymaster/"
    "system/security/keystore"
    "vendor/mediatek/proprietary/bootable/bootloader/preloader/platform/mt6755/src/security/trustzone/"
    "device/mediatek/mt6755/device.mk"
    "hardware/libhardware/include/hardware/fingerprint.h"
    "device/mediatek/common/sepolicy/init_thh.te"
    "kernel-3.18/drivers/mmc/host/mediatek/emmc_rpmb.c"
    "kernel-3.18/drivers/mmc/host/mediatek/emmc_rpmb.h"
    "device/mediatek/common/sepolicy/meta_tst.te"
    "device/mediatek/common/sepolicy/shell.te"
    "device/mediatek/mt6755/init.microtrust.rc"
    "vendor/mediatek/proprietary/hardware/meta/common/src/FtModule.cpp"
    "meios/MEIOS_Bsp/config/init.meitudriver.in"
    "vendor/mediatek/proprietary/bootable/bootloader/preloader/Android.mk"
    "vendor/mediatek/proprietary/bootable/bootloader/preloader/platform/mt6755/makefile.mak"
    "vendor/mediatek/proprietary/bootable/bootloader/preloader/platform/mt6755/src/drivers/device_apc.c"
    "kernel-3.18/drivers/misc/mediatek/spi"
    "system/core/rootdir/ueventd.rc"
   "frameworks/base/core/java/android/hardware/fingerprint/FingerprintManager.java"
   "frameworks/base/core/java/android/hardware/fingerprint/IFingerprintDaemon.aidl"
   "frameworks/base/core/java/android/hardware/fingerprint/IFingerprintService.aidl"
   "frameworks/base/services/core/java/com/android/server/fingerprint/"
   "system/core/fingerprintd/FingerprintDaemonProxy.cpp"
   "system/core/fingerprintd/FingerprintDaemonProxy.h"
   "system/core/fingerprintd/IFingerprintDaemon.cpp"
   "system/core/fingerprintd/IFingerprintDaemon.h"
   "frameworks/base/core/java/android/hardware/fingerprint/"
   "frameworks/base/core/java/android/hardware/fingerprint/IFingerprintDaemon.aidl"
   "frameworks/base/core/java/android/hardware/fingerprint/IFingerprintService.aidl"
   "frameworks/base/services/core/java/com/android/server/fingerprint/FingerprintService.java"
   "hardware/libhardware/include/hardware/fingerprint.h"
   "system/core/fingerprintd/FingerprintDaemonProxy.cpp"
   "system/core/fingerprintd/FingerprintDaemonProxy.h"
   "system/core/fingerprintd/IFingerprintDaemon.cpp"
   "system/core/fingerprintd/IFingerprintDaemon.h"   
   "frameworks/base/core/java/android/hardware/fingerprint/FingerprintManager.java"
   "hardware/libhardware/include/hardware/fingerprint.h"
   "frameworks/base/services/core/java/com/android/server/fingerprint/FingerprintService.java"
   "frameworks/base/core/java/android/hardware/fingerprint/IFingerprintService.aidl"
   "frameworks/base/core/java/android/hardware/fingerprint/IFingerprintDaemon.aidl"
   "system/core/fingerprintd/FingerprintDaemonProxy.cpp"
   "system/core/fingerprintd/FingerprintDaemonProxy.h"
   "system/core/fingerprintd/IFingerprintDaemon.cpp"
   "system/core/fingerprintd/IFingerprintDaemon.h"   
)


DATE_STR=`date +%Y%m%d_%H%M`
SVN_STR=`svnversion`

#echo ${OUT_FILE_PATH_LIST[*]} 
TAR_FILE_NAME=to_wind_fpc_tee_svn${SVN_STR}_${DATE_STR}.tar.bz2
tar -jcvf ~/meitu/mica_M/${TAR_FILE_NAME} ${OUT_FILE_PATH_LIST[*]}
