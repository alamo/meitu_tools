#!/bin/bash 

EXT_CSD_FILE=/sys/kernel/debug/mmc0/mmc0:0001/ext_csd
OLD_VERSION=00
OLD_VERSION_02=01
NEW_VERSION=02

get_extcsd()
{
    ext_csd=`adb shell cat $EXT_CSD_FILE`
}

check_version()
{
  if [ "$1" == "$OLD_VERSION" ]; then
    setprop gn.firmware.ver 00
  elif [ "$1" == "$OLD_VERSION_02" ]; then
    setprop gn.firmware.ver 01
  elif [ "$1" == "$NEW_VERSION" ]; then
    setprop gn.firmware.ver 02
  fi
}

get_extcsd
fw_version=${ext_csd:$((511*2-257*2)):2}
echo "fw version = ${fw_version}"
#check_version $fw_version
