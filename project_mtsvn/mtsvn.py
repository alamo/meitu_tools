#!/usr/bin/python
#coding=utf-8

#################################################################################################
#                                
#                                mtsvn v0.1
#    v0.1:
#    说明：commit(提交辅助) genpatch(产生补丁) update(更新当前项目到最新SVN版本) help
#          会在当前目录下生成配置文件 .commit_cfg  请加入.gitignore文件中
#
#
#    使用方式： 命令行键入mtsvn + 参数，本程序可以防止在任意系统可执行的目录下
#
#
#################################################################################################


import commands
import os
import sys
import datetime
import pickle
import shutil
import string
import getopt

## const variable
MODIFY_STR="modified:"
NEWFILE_START_STR="Untracked files:"
DELETE_STR="deleted:"
CFG_FILE=".commit_cfg"

## variable
LIST_FILE_MODIFY=[]
LIST_FILE_UNTRACKED=[]
LIST_FILE_DELETE=[]



## dirs
CURRENT_WORKING_PATH=os.getcwd()
BASE_PATH="MTK BASECODE PATH"
DRV_PATH="DRV ONLY SVN PATH"
ROM_PATH="MEITU ROM SVN PATH"
PROJECT_TYPE_STR="PROJECT TYPE (drv/rom)"
LIST_DIRS=[ BASE_PATH, DRV_PATH, ROM_PATH, PROJECT_TYPE_STR ]
CFG_DIRS={}

def show_str(str):
        print "\033[1;31;40m" + str  + "\033[0m"

def copytree(src, dst, symlinks=False):
    names = os.listdir(src)
    if not os.path.isdir(dst):
        os.makedirs(dst)
          
    errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks)
            else:
                if os.path.isdir(dstname):
                    os.rmdir(dstname)
                elif os.path.isfile(dstname):
                    os.remove(dstname)
                shutil.copy2(srcname, dstname)
            # XXX What about devices, sockets etc.?
        except (IOError, os.error) as why:
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except OSError as err:
            errors.extend(err.args[0])
    try:
        shutil.copystat(src, dst)
    except WindowsError:
        # can't copy file access times on Windows
        pass
    except OSError as why:
        errors.extend((src, dst, str(why)))
    if errors:
        raise Error(errors)


## do init things
def env_init():
        dirs={}
        if os.path.exists(CFG_FILE) == False:
                show_str("env is not set! Pls set it first")
                for i in LIST_DIRS:
                        content = raw_input("Please input " + i + " :")
                        if (content != "" and os.path.exists(content)) or (content == "null"):
                                dirs[i] = content                        
                        elif i == PROJECT_TYPE_STR:        
                                dirs[i] = content
                        else:
                                show_str("error! invaild value")
                                return -1
                                
                ##write config file
                cfgfile = open(CFG_FILE, "wb")
                pickle.dump(dirs, cfgfile)
                cfgfile.close()        
        else:
                ##load config file
                cfgfile = open(CFG_FILE, "rb")
                dirs = pickle.load(cfgfile)
                cfgfile.close()
                
        return dirs
                
##change list
def file_list_to_str(list):
        str=""
        for i in list:
                str += i + " "
        return str                
        
def get_modify_file_path_from_line(line):
        for i in line.split():
                if i != "#" and i != MODIFY_STR and os.path.exists(i):
                        return i
        return -1                                

def get_delete_file_path_from_line(line):
        for i in line.split():
                if i != "#" and i != DELETE_STR:
                        return i
        return -1                
        
def get_untracked_file_path_from_line(line):
        if len(line) <= 0 or line[0] != "#":
                return -1
        for i in line.split("        "):
                if i != "#" and os.path.exists(i):
                        return i                                
        return -1                


def find_modify_str(line):
    if line.find(MODIFY_STR) != -1 or line.find("修改") != -1:
        return True;
    return False;

def find_delete_str(line):
    if line.find(DELETE_STR) != -1 or line.find("删除") != -1:
        return True;
    return False;

def find_newfile_str(line):
    if line.find(NEWFILE_START_STR) != -1 or line.find("未跟踪的文件") != -1:
        return True;
    return False;

## Found modify file and untracked file
def get_git_info():
        show_str("start get git status...")
        (status, output)=commands.getstatusoutput('git status')
        if (status != 0):
                print "do git error! please check your project"
                exit(0)
        untracked_file_start = False        
        sp_output = output.splitlines()
        for line in sp_output:
#                if(line[0] == "#"):
                        if(untracked_file_start == False):
                                if find_modify_str(line):                                
                                        result=get_modify_file_path_from_line(line)
                                        if result != -1:
                                                LIST_FILE_MODIFY.append(result)
                                elif find_delete_str(line):
                                        result=get_delete_file_path_from_line(line)
                                        if result != -1:
                                                LIST_FILE_DELETE.append(result)
                                elif find_newfile_str(line):
                                        untracked_file_start = True
                        else:
                                result=get_untracked_file_path_from_line(line)
                                if result != -1:
                                        LIST_FILE_UNTRACKED.append(result)
        

def update_svn(path):
        if not os.path.exists(path):
                show_str("[Update] Update SVN (" + path + ") Error, Path Not Exist")
                return -1
        (status, output)=commands.getstatusoutput('cd ' + path + '; svn update')
        if status == 0:                
                show_str("[Update] Update SVN (" + path + ") Finish")
        return status
        
def start_compare(path_left, path_right, commit_cmd):
        show_str("Press \"y\" or \"yes\" to compare,  other key to skyp")
        input=raw_input("Input: ");
        if input.lower() == "y" or input.lower() == "yes":
                (status, output)=commands.getstatusoutput(commit_cmd + " " + path_left + " " + path_right)
                if status == 0:                
                        show_str("Compare " + path_left + " vs " + path_right + " Finish!")
                return status
        else:
                return 0        
        
def start_bcompare(path_left, path_right):    
    start_compare(path_left, path_right, 'bcompare')    
    
def start_wincompare(path_left, path_right):    
    start_compare(path_left, path_right, 'wincompare')    
    

def check_new_file(svn_path, patch_path):
        copy_file_list=[]
        for parent,dirnames,filenames in os.walk(patch_path):
                for filename in filenames:
                        #print parent  + " " + filename      
                        join_path = os.path.join(parent,filename)
                                                #在SVN中不存在并且在BASE CODE中存在的文件被认为是基础文件
                        if os.path.exists(join_path.replace(patch_path, CFG_DIRS[BASE_PATH])) and not os.path.exists(join_path.replace(patch_path, svn_path)):
                                                        copy_file_list.append(join_path.replace(patch_path+'/', ''))
                                                        if not os.path.exists(os.path.dirname(join_path.replace(patch_path, svn_path))):
                                                                os.makedirs(os.path.dirname(join_path.replace(patch_path, svn_path)))
                                                        shutil.copyfile(join_path.replace(patch_path, CFG_DIRS[BASE_PATH]), join_path.replace(patch_path, svn_path))
        if len(copy_file_list) != 0:                   
                show_str("Need Copy new file!!!")
                print copy_file_list
                (status, output)=commands.getstatusoutput("cd " + svn_path + ';svn st | awk \'{if ( $1 == "?") { print $2}}\' | xargs svn add')
                if (status != 0):
                    print "do svn add " +  " error!" + output
                    exit(0)
                #content = raw_input("Do you want to add file to " + patch_path + ":\n")
                #if content[0] == 'Y' or content[0] == 'y':
                #    (status, output)=commands.getstatusoutput("cd " + patch_path + ';svn commit -m \"[NEW] 提交原始文件\"')
                return True                
        return False

def do_diff():
        get_git_info()

        #create diff patch
        now = datetime.datetime.now()
        svn_version = get_svn_version(os.getcwd())        
        if svn_version != -1:
                svn_str='%d' %svn_version
                PATCH_DIRS="../patchs/patch_diff_" + "svn" + svn_str + "_" + now.strftime('%Y_%m%d_%H%M%S')         
        else:
                PATCH_DIRS="../patchs/patch_diff_" + now.strftime('%Y_%m%d_%H%M%S')         

        #create backup patch for safe
        BACKUP_PATCH_DIRS=PATCH_DIRS+"_backup"
        gen_patch(BACKUP_PATCH_DIRS, LIST_FILE_MODIFY+LIST_FILE_UNTRACKED)        
        #do git stash        
        show_str("start do git stash");
        (status, output)=commands.getstatusoutput('git stash')
        if (status != 0):
                show_str("do git stash error! please check your project and " + BACKUP_PATCH_DIRS) 
        #create origin patch
        gen_patch(PATCH_DIRS, LIST_FILE_MODIFY+LIST_FILE_UNTRACKED)        
        #do git apply stash
        show_str("start do git stash apply");
        (status, output)=commands.getstatusoutput('git stash apply')
        if (status != 0):
                show_str("do git stash apply error! please check your project and " + BACKUP_PATCH_DIRS)
        #start bcompare        
        start_bcompare("./",  PATCH_DIRS)
        
def do_smartcommit():
        show_str("smart commit")
        get_git_info()        
        ##
        gen_path="../patchs/patch_smartcommit" + "_" + datetime.datetime.now().strftime('%Y_%m%d_%H%M%S')
        show_str("Genpatch to " + gen_path)
        if gen_patch(gen_path, LIST_FILE_MODIFY+LIST_FILE_UNTRACKED) != 0:
                return -1
                
        ## commit rom
        show_str("Updating ROM svn to " + CFG_DIRS[ROM_PATH]);
        if CFG_DIRS[ROM_PATH] != "null":
                if update_svn(CFG_DIRS[ROM_PATH]) != 0:         
                        show_str("update rom svn error")
                        return -1
                if check_new_file(CFG_DIRS[ROM_PATH], gen_path):
                        show_str("#### please commit rom new file first!!! ####")
                        raw_input("Press Any Key to Continue after commit");
                if start_bcompare(CFG_DIRS[ROM_PATH], gen_path) != 0:
                        return -1                

        ## commit drv
        show_str("Updating DRV svn to " + CFG_DIRS[DRV_PATH]);
        if CFG_DIRS[DRV_PATH] != "null":
                if update_svn(CFG_DIRS[DRV_PATH]) != 0:
                        show_str("update drv svn error")
                        return -1
                if check_new_file(CFG_DIRS[DRV_PATH], gen_path):
                        show_str("#### please commit drv new file first!!! ####")
                        raw_input("Press Any Key to Continue after commit");
                if start_bcompare(CFG_DIRS[DRV_PATH], gen_path) != 0:
                        return -1


                
def __do_commit(commit_cmd):        
        show_str("commit")
        get_git_info()        
        ##
        gen_path="../patchs/patch_commit" + "_" + datetime.datetime.now().strftime('%Y_%m%d_%H%M%S')
        show_str("Genpatch to " + gen_path)
        
        do_git_tar_patch(gen_path)
        #patch_cmd="git status -s | awk '{print $2}'|xargs tar -jcvf - | tar -jxvf - -C " + gen_path
        #if gen_patch(gen_path, LIST_FILE_MODIFY+LIST_FILE_UNTRACKED) != 0:
        #        return -1
                
        ## commit rom
        show_str("Updating ROM svn to " + CFG_DIRS[ROM_PATH]);
        if CFG_DIRS[ROM_PATH] != "null":
                if update_svn(CFG_DIRS[ROM_PATH]) != 0:         
                        show_str("update rom svn error")
                        return -1
                if check_new_file(CFG_DIRS[ROM_PATH], gen_path):
                        show_str("#### please commit rom new file first!!! ####")
                        raw_input("Press Any Key to Continue after commit");
                if start_compare(CFG_DIRS[ROM_PATH], gen_path, commit_cmd) != 0:
                        return -1                

        ## commit drv
        show_str("Updating DRV svn to " + CFG_DIRS[DRV_PATH]);
        if CFG_DIRS[DRV_PATH] != "null":
                if update_svn(CFG_DIRS[DRV_PATH]) != 0:
                        show_str("update drv svn error")
                        return -1
                if check_new_file(CFG_DIRS[DRV_PATH], gen_path):
                        show_str("#### please commit drv new file first!!! ####")
                        raw_input("Press Any Key to Continue after commit");
                if start_compare(CFG_DIRS[DRV_PATH], gen_path, commit_cmd) != 0:
                        return -1
                


def do_commit():
    __do_commit('bcompare')

def do_wincommit():
    __do_commit('wincompare')
    
    
def gen_patch(patch_dir, file_list):
        if os.path.exists(patch_dir) == False:
                os.makedirs(patch_dir)
        
        for file in file_list:        
                target_path = os.path.join(patch_dir, file)
                if not os.path.exists(os.path.dirname(target_path)):
                        os.makedirs(os.path.dirname(target_path))
                if os.path.isdir(file):
                        copytree(file, target_path, True)
                else:
                        shutil.copy2(file, target_path)        
                
        return 0        
        #str=file_list_to_str(file_list)
        #if str != "":
        #        (status, output)=commands.getstatusoutput('tar -cvf - ' + str + ' | tar -xvf - -C ' + patch_dir)
        #        if status != 0:
        #                show_str("gen patch error")                        
        #        return status
        #else:
        #        show_str("No file need to gen")
        #        return -1
                
#def do_genpatch():
#       get_git_info()
#       #create patchs
#       content = raw_input("Please input patch tag:")
#       if content == "":
#               content = "unknow"
#       now = datetime.datetime.now()
#       svn_version = get_svn_version(os.getcwd())        
#       if svn_version != -1:
#               svn_str='%d' %svn_version
#               PATCH_DIRS="../patchs/patch_" + content + "_" + "svn" + svn_str + "_" + now.strftime('%Y_%m%d_%H%M%S')         
#       else:
#               PATCH_DIRS="../patchs/patch_" + content + "_" + now.strftime('%Y_%m%d_%H%M%S')         
#       
#       #create patch
#       gen_patch(PATCH_DIRS, LIST_FILE_MODIFY+LIST_FILE_UNTRACKED)    

def do_git_tar_patch(path):
        patch_cmd="git status -s | awk '{print $2}'|xargs tar -jcvf - | tar -jxvf - -C " + path
        show_str(patch_cmd)
        if os.path.exists(path) == False:
                os.makedirs(path)
                
        (status, output) = commands.getstatusoutput(patch_cmd)
        if (status != 0):
                return -1  
        return 0
        
def do_genpatch():
        #create patchs
        content = raw_input("Please input patch tag:")
        if content == "":
                content = "unknow"
        now = datetime.datetime.now()
        svn_version = get_svn_version(os.getcwd())        
        if svn_version != -1:
                svn_str='%d' %svn_version
                PATCH_DIRS="../patchs/patch_" + content + "_" + "svn" + svn_str + "_" + now.strftime('%Y_%m%d_%H%M%S')         
        else:
                PATCH_DIRS="../patchs/patch_" + content + "_" + now.strftime('%Y_%m%d_%H%M%S')         
        
        #create patch
        ret = do_git_tar_patch(PATCH_DIRS)
        if ret < 0:
            show_str("genpatch error")
            return ret

        show_str("genpatch success at " + PATCH_DIRS)
        return 0
        
def get_svn_version(path):
        (status, output) = commands.getstatusoutput("svn info " + path)
        if (status != 0):
                show_str("check svn info error")
                return -1        
        for line in output.splitlines():        
                ver_start = line.find("版本: ")                
                if ver_start != -1:                
                        svn_num = string.atoi(line[ver_start+len("版本: "):])
                        return svn_num
                else:
                    ver_start = line.find("Revision: ")
                    if ver_start != -1:
                        svn_num = string.atoi(line[ver_start+len("Revision: "):])
                        return svn_num                        
        return -1                                
                                
def __do_update(is_svn_update):        
        PROJECT_SVN_VERSION = get_svn_version(os.getcwd())                
        show_str("start updating....")        
        svn_path = CFG_DIRS[ROM_PATH]        
        if CFG_DIRS.get(PROJECT_TYPE_STR, "rom") != "rom":
                svn_path = CFG_DIRS[DRV_PATH]        
        if update_svn(svn_path) != 0:         
                show_str("update " + svn_path + " error")
                return -1                                        
                
        #chk need to update
        print svn_path
        PKG_SVN_VERSION = get_svn_version(svn_path)        
        if PKG_SVN_VERSION <= PROJECT_SVN_VERSION:
                show_str("project version (%d"%PROJECT_SVN_VERSION + ") >= svn verson(%d"%PKG_SVN_VERSION + ")")
                return 0

        get_git_info()
        gen_path="../patchs/patch_update" + "_" + datetime.datetime.now().strftime('%Y_%m%d_%H%M%S')
        if gen_patch(gen_path, LIST_FILE_MODIFY+LIST_FILE_UNTRACKED) != 0:
                return -1
                
        # i not sure if "git stash" will delete new files, we should check it later 
        (status, output)=commands.getstatusoutput('git stash')
        if (status != 0):
                show_str("do git stash error! please check your project")
                return -1
        
        if (is_svn_update == True):
                show_str("start svn update....")
                (status, output)=commands.getstatusoutput('svn update')
                if status == 0:
                        show_str("[Update] Update SVN (" + svn_path + ") Finish")
        else:
                show_str("start copying....")
                copytree(svn_path, ".", True)

        ##update meios
        MEIOS_SVN_VERSION=""
        #if os.path.exists("meios"):
        #        MEIOS_PATH="./meios"
        #        if update_svn(MEIOS_PATH) != 0:
        #                show_str("update " + MEIOS_PATH + " error")
        #                return -1
        #        MEIOS_SVN_VERSION = get_svn_version(MEIOS_PATH)
                        
        show_str("start add file to git")        
        (status, output)=commands.getstatusoutput('git add -A')
        if (status != 0):
                print "do git stash error! please check your project"
                return -1                
        #git commit                                
        if MEIOS_SVN_VERSION == "":
                commit_attr = "update to svn %d"%PKG_SVN_VERSION
        else:
                commit_attr = "update to svn %d"%PKG_SVN_VERSION + ", meios %d"%MEIOS_SVN_VERSION        
        
        show_str(commit_attr)
        (status, output)=commands.getstatusoutput('git commit -am \"' + commit_attr + "\"")
        if (status != 0):
                show_str("do git commit error! please check your project. status = %d"%status)
                return -1
        #compare file with project
        if start_bcompare(".", gen_path) != 0:
                return -1

def do_update():
    return __do_update(False)
        
def do_svn_update():        
    return __do_update(True)  
        
def do_info():        
        for i in LIST_DIRS:
                print "\033[1;31;40m" + i + "\033[0m" + " is " + "\033[1;31;40m" + CFG_DIRS[i] + "\033[0m"

def do_set_info():
#LIST_DIRS=[ BASE_PATH, DRV_PATH, ROM_PATH, PROJECT_TYPE_STR ]
#-base BASE_PATH -drv DRV_PATH -rom ROM_PATH -type drv/rom
        setdirs = {}
        opts, args = getopt.getopt(sys.argv[2:], "b:d:r:t:", ["base=", "drv=", "rom=", "type="]) 
        print opts
        print args
        for a,o in opts: 
                print "a = " + a
                if a in ('-b', '--base'): 
                        if (o != "" and os.path.exists(o)) or (o == "null"):
                                base_path = o
                                setdirs[BASE_PATH] = o
                        else:
                                print "SET PATH [" + a + "] not exist"
                                return
                elif a in ('-d', '--drv'):
                        if (o != "" and os.path.exists(o)) or (o == "null"):        
                                drv_path = o 
                                setdirs[DRV_PATH] = o
                        else:
                                print "SET PATH " + a + " ERROR"
                                return
                elif a in ('-r', '--rom'): 
                        if (o != "" and os.path.exists(o)) or (o == "null"):                
                                rom_path = o 
                                setdirs[ROM_PATH] = o
                        else:
                                print "SET PATH " + a + " ERROR"
                                return
                elif a in ('-t', '--type'):
                        type = o
                        setdirs[PROJECT_TYPE_STR] = o
        
        if len(setdirs) == 0:
                print "Set info Error "
                return 
                
        for i in LIST_DIRS:
                print setdirs[i]
                if setdirs[i] == "":
                        return
        
        print setdirs
        
        ##write config file
        cfgfile = open(CFG_FILE, "wb")
        pickle.dump(setdirs, cfgfile)
        cfgfile.close()        
        print "Set info Success!"
        
def do_help():
        print ACTIONS.keys()


        
ACTIONS = {  
"commit":do_commit,        
"wincommit":do_wincommit,        
"smartcommit":do_smartcommit,
"diff":do_diff,        
"genpatch":do_genpatch,
"update":do_update,
"svn_update":do_svn_update,
"set_info":do_set_info,
"info":do_info,
"help":do_help
}        
                                
#### main ####                
if len(sys.argv) > 1: 
        if (sys.argv[1] != "set_info" and sys.argv[1] != "genpatch"):
                CFG_DIRS = env_init()
                if CFG_DIRS == -1:
                        exit()
        ACTIONS.get(sys.argv[1])()
        exit()

do_help()        




