#ifndef __DAKEY__
#define __DAKEY__
#define DA_PUBK_SZ 256
#define DA_PUBK   0xE8, 0xF6, 0x5C, 0xF9, 0x2D, 0x3F, 0x63, 0x18, 0x92, 0x66, 0x24, 0x2F, 0x81, 0xEE, 0x8A, 0xFC, \
                   0x3A, 0xF1, 0x07, 0x5D, 0x99, 0x15, 0xF9, 0x34, 0x2F, 0xFF, 0x3E, 0x57, 0x0C, 0x0A, 0xAD, 0xB1, \
                   0x40, 0x66, 0xF9, 0x68, 0x51, 0x69, 0xAD, 0x7A, 0x57, 0x92, 0xF2, 0xFC, 0xA4, 0xF5, 0x4B, 0x35, \
                   0x8B, 0x32, 0x35, 0x52, 0xFB, 0xE1, 0x96, 0xC6, 0x83, 0xE8, 0x86, 0x63, 0xD7, 0xCC, 0xBC, 0x14, \
                   0x4F, 0xE8, 0xFB, 0x6A, 0xC0, 0x74, 0x33, 0xCB, 0x49, 0x09, 0x63, 0x5E, 0xED, 0x8B, 0x66, 0x5F, \
                   0xD6, 0x4C, 0xB2, 0x35, 0x60, 0x88, 0x50, 0xE6, 0xF3, 0xDD, 0x5F, 0x7E, 0x09, 0x4B, 0xF8, 0x42, \
                   0xA2, 0x7E, 0x5B, 0x49, 0x37, 0x1B, 0xB1, 0x94, 0x06, 0xA5, 0x0A, 0xF8, 0xAB, 0x4F, 0x5A, 0x14, \
                   0x3F, 0xF3, 0x55, 0xED, 0xF2, 0x4B, 0x05, 0x70, 0x59, 0xE5, 0x0D, 0xB5, 0xBC, 0x86, 0xA8, 0x9C, \
                   0xEF, 0xE7, 0x51, 0x7A, 0x15, 0xA9, 0xF4, 0x4D, 0xD1, 0x14, 0x68, 0x4E, 0xEE, 0xEE, 0xBB, 0x4A, \
                   0x06, 0x20, 0x6B, 0x50, 0xA5, 0x28, 0x22, 0x4C, 0xBC, 0xD8, 0xC3, 0xD9, 0xFF, 0xB8, 0x34, 0x95, \
                   0xC6, 0x7C, 0x8B, 0x99, 0x83, 0x8F, 0x58, 0x82, 0x6E, 0x88, 0x2D, 0x13, 0xE5, 0xAA, 0x30, 0xA1, \
                   0x3F, 0x53, 0x87, 0xD7, 0x36, 0xD2, 0x97, 0xDD, 0x10, 0x4F, 0x5B, 0xF9, 0xB2, 0xB1, 0x34, 0xB1, \
                   0xBE, 0x54, 0xFC, 0xE0, 0xBE, 0xC7, 0x00, 0xA4, 0x40, 0xDB, 0x7E, 0xE5, 0x95, 0xC0, 0x99, 0x7B, \
                   0xBA, 0xBE, 0x7B, 0x66, 0x40, 0x21, 0xA4, 0x53, 0x2F, 0x92, 0xD5, 0x47, 0x41, 0x3C, 0xB3, 0xCD, \
                   0xF3, 0xDC, 0x64, 0xF3, 0x94, 0x7A, 0x6B, 0x4F, 0xD8, 0x5A, 0x19, 0x57, 0x8C, 0x0D, 0xF6, 0x65, \
                   0x10, 0xB8, 0xB7, 0xBB, 0xA3, 0x7B, 0xFB, 0xED, 0x73, 0x1A, 0x62, 0xBA, 0x87, 0x77, 0x6E, 0x9D
#endif /*__DAKEYY__ */
